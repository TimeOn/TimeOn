## Project Timetable ##

#### Локальный запуск с использованием Docker'а ####

    mvn clean package
    docker build -t registry.gitlab.com/timeon/timeon:master .
    docker-compose up -d

##### Для просмотра логов контейнера #####

    docker ps
    docker logs <id_контейнера>
    
##### Для остановки контейнеров #####

    docker-compose stop
