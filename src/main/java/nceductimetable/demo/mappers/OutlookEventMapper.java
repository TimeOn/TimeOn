package nceductimetable.demo.mappers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jdk.nashorn.internal.ir.annotations.Ignore;
import nceductimetable.demo.dtoOutlook.EventOutlookDTOPost;
import nceductimetable.demo.dtoPost.EventDTOPost;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper()
public interface OutlookEventMapper {

    @Mappings({
            @Mapping(target = "subject", source = "eventDTOPost.name"),
            @Mapping(target = "body.content", source = "eventDTOPost.description"),
            @Mapping(target = "start.dateTime", source = "eventDTOPost.dateTimeBegin"),
            @Mapping(target = "end.dateTime", source = "eventDTOPost.dateTimeEnd"),
            @Mapping(target = "start.timeZone", source = "eventDTOPost.timeZone"),
            @Mapping(target = "end.timeZone", source = "eventDTOPost.timeZone"),
            @Mapping(target = "location.displayName", source = "eventDTOPost.address"),
            @Mapping(target = "recurrence", ignore = true)


    })
    EventOutlookDTOPost eventToEventOutlook(EventDTOPost eventDTOPost);
}
