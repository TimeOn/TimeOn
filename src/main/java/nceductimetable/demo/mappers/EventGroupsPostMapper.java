package nceductimetable.demo.mappers;


import nceductimetable.demo.dtoPost.EventGroupsDTOPost;
import nceductimetable.demo.model.EventGroups;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface EventGroupsPostMapper {
    @Mappings({

            @Mapping(target = "group.id", source = "group")
    })
    EventGroups eventGroupsDTOPosttoEventGroups(EventGroupsDTOPost eventGroupsDTO);

    @Mappings({

            @Mapping(target = "group", source = "group.id")
    })
    EventGroupsDTOPost eventGroupsToEventGroupsDTOPost(EventGroups eventGroups);

}
