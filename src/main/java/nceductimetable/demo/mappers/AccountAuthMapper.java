package nceductimetable.demo.mappers;

import nceductimetable.demo.dto.AccountAuth;
import nceductimetable.demo.model.Account;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface AccountAuthMapper {

    @Mappings({
            @Mapping(target = "login", source = "accountAuth.username"),
            @Mapping(target = "activated", source = "accountAuth.enabled")
    })
    Account accountAuthToAccount(AccountAuth accountAuth);

    @Mappings({
            @Mapping(target = "username", source = "account.login"),
            @Mapping(target = "enabled", source = "account.activated")
    })
    AccountAuth accountToAccountAuth(Account account);
}
