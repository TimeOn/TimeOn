package nceductimetable.demo.mappers;

import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.model.Event;
import org.mapstruct.Mapper;

@Mapper(uses= {EventAccountsPostMapper.class, EventGroupsPostMapper.class})
public interface EventDTOPostMapper {
    Event eventDTOPostToEvent(EventDTOPost eventDTOPost);

    EventDTOPost eventToEventDTOPost(Event event);
}
