package nceductimetable.demo.mappers;

import nceductimetable.demo.dtoOutlook.Attendee;
import nceductimetable.demo.dtoPost.EventAccountsDTOPost;
import nceductimetable.demo.model.EventAccounts;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface OutlookAttendeeMapper {
    @Mappings({
            @Mapping(target = "emailAddress.address", source = "accountsDTOPost.login"),
            @Mapping(target = "emailAddress.name", source = "accountsDTOPost.name")
    })
    Attendee accountsToAttendee(EventAccountsDTOPost accountsDTOPost);
}
