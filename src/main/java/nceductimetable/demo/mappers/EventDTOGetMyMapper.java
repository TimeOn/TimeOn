package nceductimetable.demo.mappers;

import nceductimetable.demo.dtoGet.EventDTOGetMy;
import nceductimetable.demo.model.Event;
import org.mapstruct.Mapper;

@Mapper(uses= {EventAccountsPostMapper.class, EventGroupsPostMapper.class})
public interface EventDTOGetMyMapper {
    Event eventDTOGetMyToEvent(EventDTOGetMy eventDTOGet);

    EventDTOGetMy eventToEventDTOGetMy(Event event);
}
