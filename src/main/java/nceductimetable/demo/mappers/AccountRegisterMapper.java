package nceductimetable.demo.mappers;

import nceductimetable.demo.dto.AccountRegister;
import nceductimetable.demo.model.Account;
import org.mapstruct.Mapper;

@Mapper
public interface AccountRegisterMapper {

    Account accountRegistertoAccount(AccountRegister accountAuth);

    AccountRegister accountToAccountRegister(Account account);
}
