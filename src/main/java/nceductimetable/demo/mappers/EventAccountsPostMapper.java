package nceductimetable.demo.mappers;


import nceductimetable.demo.dtoPost.EventAccountsDTOPost;
import nceductimetable.demo.model.EventAccounts;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface EventAccountsPostMapper {

    EventAccounts eventAccountsDTOPosttoEventAccounts(EventAccountsDTOPost eventAccountsDTO);


    EventAccountsDTOPost eventAccountsToEventAccountsDTOPost(EventAccounts eventAccounts);

}
