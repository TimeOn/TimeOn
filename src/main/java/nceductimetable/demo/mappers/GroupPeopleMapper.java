package nceductimetable.demo.mappers;

import nceductimetable.demo.dto.GroupPeopleDTO;
import nceductimetable.demo.dto.GroupPeopleDTOGet;
import nceductimetable.demo.model.GroupPeople;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface GroupPeopleMapper {
    @Mappings({
            @Mapping(target = "name", source = "groupPeople.name"),
            @Mapping(target = "description", source = "groupPeople.description"),
            @Mapping(target = "members", source = "groupPeople.members")
    })
    GroupPeopleDTO groupPeopleToGroupPeopleDTO(GroupPeople groupPeople);

    @Mappings({
            @Mapping(target = "name", source = "groupPeople.name"),
            @Mapping(target = "description", source = "groupPeople.description"),
            @Mapping(target = "members", source = "groupPeople.members")
    })
    GroupPeopleDTOGet groupToDroupDTOGet(GroupPeople groupPeople);

}
