package nceductimetable.demo.mappers;

import nceductimetable.demo.dtoGet.ExternalAccountDTOGet;
import nceductimetable.demo.model.ExternalAccount;
import org.mapstruct.Mapper;

@Mapper
public interface ExternalAccountMapper {
    ExternalAccount externalAccountDTOGetToExternalAccount(ExternalAccountDTOGet externalAccountDTOGet);
    ExternalAccountDTOGet externalAccountToExternalAccountDTOGet(ExternalAccount externalAccount);
}
