package nceductimetable.demo.repositories;

import nceductimetable.demo.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

import static org.springframework.orm.hibernate3.SessionFactoryUtils.getSession;

public interface AccountRepository extends CrudRepository<Account, Integer> {
    Account findAccountByLogin(String login);

    /*@Query("SELECT a" +
            "FROM Account a" +
            "WHERE to_tsvector(a.name || ' ' || a.surname || ' ' || a.login) @@ to_tsquery(:searchRequest)" +
            "ORDER BY a.id DESC LIMIT :count, :offset;")
*/
    /*@Query(//nativeQuery = true,
            value ="SELECT o " +
            "FROM Account o WHERE o.login like concat( :searchRequest, '%') "// +
                    //":count " //+
                   // "LIMIT :count, :offset"
    )*/
    List<Account> findByLoginLikeIgnoreCase(String login,Pageable pageable);//, Pageable pageable);
    //List<Account> findAccounts(@Param("searchRequest")String searchRequest, Pageable page);
    //public List<Account> searchAccounts(@Param("searchRequest")String searchRequest//,
                                                       // @Param("count")int count//,
                                                        //@Param("offset")int offset
   // );


}
