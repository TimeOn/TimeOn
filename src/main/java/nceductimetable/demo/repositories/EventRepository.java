package nceductimetable.demo.repositories;

import nceductimetable.demo.dtoGet.EventWithReccurrence;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.model.Event;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


public interface EventRepository extends CrudRepository<Event, Integer> {
    /*@Query(nativeQuery = true,
            value="SELECT * FROM unnest(get_occurrences(cast(:recurrence as rrule), cast(:dateTime as timestamp)))")
    List<Timestamp> getOccurrences(@Param("recurrence") String recurrence, @Param("dateTime") String dateTime);
*/



    @Query(nativeQuery=true,
    value="SELECT events.*,  events.start_time + make_interval(0,0,0,0,0,0,cast(events.duration / 1000 as int)) " +
            "FROM" +
            "  (SELECT e.*, unnest(get_occurrences(cast(e.recurrence as rrule), cast(e.date_time_begin as timestamp))) start_time" +
            "  FROM event e) events " +
            "WHERE events.start_time < cast(:end as timestamp) AND events.id IN" +
            "(SELECT ce.event_id FROM calendar_event ce WHERE ce.calendar_id= :calId)")
    List<EventWithReccurrence> getOccurrences(@Param("end") String end, @Param("calId") Integer calId
     );

}
