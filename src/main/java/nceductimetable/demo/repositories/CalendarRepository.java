package nceductimetable.demo.repositories;

import nceductimetable.demo.model.Account;
import nceductimetable.demo.model.Calendar;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CalendarRepository extends CrudRepository<Calendar, Integer> {
    public List<Calendar> findCalendarByAccount(Account account);
}
