package nceductimetable.demo.repositories;

import nceductimetable.demo.model.Calendar;
import nceductimetable.demo.model.CalendarEvent;
import nceductimetable.demo.model.Event;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CalendarEventRepository extends CrudRepository<CalendarEvent, Integer> {
    List<CalendarEvent> findCalendarEventByExternalEventId(String externalEventId);
    List<CalendarEvent> findCalendarEventByEvent(Event event);
    CalendarEvent findCalendarEventByCalendarAndEvent(Calendar calendar, Event event);
    CalendarEvent findCalendarEventByCalendarAndExternalEventId(Calendar calendar, String externalEventId);
}
