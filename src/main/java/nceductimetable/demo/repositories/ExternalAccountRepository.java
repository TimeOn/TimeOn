package nceductimetable.demo.repositories;

import nceductimetable.demo.model.Account;
import nceductimetable.demo.model.ExternalAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ExternalAccountRepository extends CrudRepository<ExternalAccount, Integer> {
    ExternalAccount findExternalAccountByUserEmail(String mail);
    List<ExternalAccount> findExternalAccountsByAccount(Account account);
    ExternalAccount findExternalAccountByUserName(String name);
}
