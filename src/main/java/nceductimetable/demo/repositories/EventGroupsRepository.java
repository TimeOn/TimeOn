package nceductimetable.demo.repositories;

import nceductimetable.demo.model.EventGroups;
import org.springframework.data.repository.CrudRepository;

public interface EventGroupsRepository extends CrudRepository<EventGroups, Integer> {
}
