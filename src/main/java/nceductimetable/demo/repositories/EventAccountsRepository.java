package nceductimetable.demo.repositories;

import nceductimetable.demo.model.Account;
import nceductimetable.demo.model.Event;
import nceductimetable.demo.model.EventAccounts;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface EventAccountsRepository extends CrudRepository<EventAccounts, Integer> {
EventAccounts findByEventAndAccount(Event event, Account account);

}
