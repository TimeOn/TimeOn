package nceductimetable.demo.repositories;

import nceductimetable.demo.model.EventAccounts;
import nceductimetable.demo.model.EventExternalAccounts;
import org.springframework.data.repository.CrudRepository;

public interface EventExternalAccountsRepository extends CrudRepository<EventExternalAccounts, Integer> {
}
