package nceductimetable.demo.repositories;

import nceductimetable.demo.model.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<Account, Integer> {

    @Query("select o from Account o where o.login = :login")
    Optional<Account> getAccountByLogin(@Param("login") String login);

    @Query("select o from Account o where o.email = :mail")
    Optional<Account> getAccountByMail(@Param("mail") String mail);

    @Query("select o from Account o where o.activate = :activate")
    Optional<Account> getAccountByActivateCode(@Param("activate") String activate);
}

