package nceductimetable.demo.repositories;

import nceductimetable.demo.model.Account;
import nceductimetable.demo.model.GroupPeople;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GroupPeopleRepository extends CrudRepository<GroupPeople, Integer> {
    GroupPeople findGroupPeopleByName(String name);
    List<GroupPeople> findGroupPeopleByMembers(Account account);
    List<GroupPeople> findByNameLikeIgnoreCase(String name);
}
