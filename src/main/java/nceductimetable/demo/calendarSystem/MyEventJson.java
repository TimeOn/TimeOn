package nceductimetable.demo.calendarSystem;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import nceductimetable.demo.dtoOutlook.*;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MyEventJson {
    private String id;
    private String subject;
    private String bodyPreview;
    private Recipient organizer;
    private List<Attendee> attendees;
    private StartOrEnd start;
    private StartOrEnd end;
    private Location location;

}
