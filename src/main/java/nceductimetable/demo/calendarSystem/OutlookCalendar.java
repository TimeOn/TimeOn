package nceductimetable.demo.calendarSystem;

import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.calendarSystem.oauth.AuthHelper;
import nceductimetable.demo.calendarSystem.oauth.TokenResponse;
import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.dtoGet.EventDTOGetMy;
import nceductimetable.demo.dtoGet.EventDTOGetNotMy;
import nceductimetable.demo.dtoOutlook.*;
import nceductimetable.demo.dtoPost.EventAccountsDTOPost;
import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.enumForEvents.EventMembersStatus;
import nceductimetable.demo.enumForEvents.ExternalSystem;
import nceductimetable.demo.exceptions.OutlookException;
import nceductimetable.demo.mappers.OutlookEventMapper;
import nceductimetable.demo.model.Calendar;
import nceductimetable.demo.model.ExternalAccount;
import nceductimetable.demo.repositories.ExternalAccountRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.web.bind.annotation.ResponseStatus;
import retrofit2.Response;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;


@Slf4j
public class OutlookCalendar implements CalendarSystem {
    private ExternalAccountRepository externalAccountRepository;

    public OutlookCalendar(ExternalAccountRepository externalAccountRepository) {
        this.externalAccountRepository = externalAccountRepository;
    }

    public MyEventJson getEventById(Calendar calendar, String eventId, String timeZone) {
        ExternalAccount externalAccount = calendar.getExternalAccount();
        TokenResponse tokens = externalAccount.getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = externalAccount.getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        String email = externalAccount.getUserEmail();
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.of(timeZone));
        String calendarId = calendar.getExternalId();
        String properties = "id, subject,organizer,attendees,start,end,location ";
        externalAccount.setToken(tokens);
        externalAccountRepository.save(externalAccount);

        try {
            MyEventJson eventJson = outlookService.getEvent(eventId, properties).execute().body();
            if (eventJson != null)
                return eventJson;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateEvent(Calendar calendar, EventDTOPost eventDTOPost, String eventId) {
        OutlookEventMapper outlookEventMapper = Mappers.getMapper(OutlookEventMapper.class);
        //ObjectMapper objectMapper = new ObjectMapper();//no
        EventOutlookDTOPost eventToCreate = new EventOutlookDTOPost();
        eventToCreate = outlookEventMapper.eventToEventOutlook(eventDTOPost);
        Pattern pattern = new Pattern();
        Range range = new Range();
        if (eventDTOPost.getRecurrence() != null) {
            String[] strings = eventDTOPost.getRecurrence().split(";");
            for (String part : strings) {
                String pieces[] = part.split("=");
                switch (pieces[0]) {
                    case "FREQ":
                        pattern.setType(pieces[1]);
                        break;
                    case "INTERVAL":
                        pattern.setInterval(Integer.parseInt(pieces[1]));
                        break;
                    case "BYDAY": {
                        String days[] = pieces[1].split(",");
                        List<String> newDays = new ArrayList<>();
                        for (String day : days) {
                            switch (day) {
                                case "MO":
                                    newDays.add("monday");
                                    break;
                                case "TU":
                                    newDays.add("tuesday");
                                    break;
                                case "WE":
                                    newDays.add("wednesday");
                                    break;
                                case "TH":
                                    newDays.add("thursday");
                                    break;
                                case "FR":
                                    newDays.add("friday");
                                    break;
                                case "SA":
                                    newDays.add("saturday");
                                    break;
                                case "SU":
                                    newDays.add("sunday");
                                    break;
                            }
                        }
                        //String[] str=newDays.toArray(new String[] {});
                        pattern.setDaysOfWeek(newDays.toArray(new String[]{}));
                        break;
                    }
                    case "UNTIL": {
                        range.setType(RecurrenceRangeTypes.endDate.name());
                        DateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
                        Date date = null;
                        try {
                            date = format.parse(pieces[1]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        range.setEndDate(date);

                        break;
                    }
                    case "COUNT": {
                        range.setType(RecurrenceRangeTypes.numbered.name());
                        range.setNumberOfOccurrences(Integer.parseInt(pieces[1]));
                    }

                }
            }
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            try {
                Date date = format.parse(eventDTOPost.getDateTimeBegin().toLocalDate().toString());
                range.setStartDate(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // range.setStartDate(new Date(eventDTOPost.getDateTimeBegin()
            //       .toInstant(ZoneOffset.of(ZoneId.of(eventDTOPost.getTimeZone()))).toEpochMilli()));
            //java.sql.Timestamp.valueOf( eventDTOPost.getDateTimeBegin()));//.toInstant(ZoneOffset.UTC)));
            if (range.getType() == null)
                range.setType(RecurrenceRangeTypes.noEnd.name());
            Recurrence recurrence = new Recurrence();
            recurrence.setPattern(pattern);
            recurrence.setRange(range);
            eventToCreate.setRecurrence(recurrence);//objectMapper.readValue(eventDTOPost.getRecurrence(), Recurrence.class));
        }

        Attendee attendee = new Attendee();
        attendee.setType("required");
        EmailAddress emailAddress = new EmailAddress();
        emailAddress.setName(calendar.getExternalAccount().getUserName());
        emailAddress.setAddress(calendar.getExternalAccount().getUserEmail());
        attendee.setEmailAddress(emailAddress);
        List<Attendee> attendees = new ArrayList<>();
        //attendees.add(attendee);

        if (eventDTOPost.getMembersAccounts() != null) {

            for (EventAccountsDTOPost member : eventDTOPost.getMembersAccounts()) {
                log.info("{}", member.getExtSystem());
                log.info("{}", member.getLogin());
                log.info("{}", member.getName());
                if (member.getExtSystem() != null)
                    if (member.getExtSystem().equals(ExternalSystem.OUTLOOK.name())) {

                        Attendee attendee2 = new Attendee();
                        if (member.isImportant())
                            attendee2.setType("required");
                        else attendee2.setType("optional");
                        EmailAddress emailAddress2 = new EmailAddress();
                        emailAddress2.setName(member.getName());
                        emailAddress2.setAddress(member.getLogin());
                        attendee2.setEmailAddress(emailAddress2);
                        attendees.add(attendee2);
                    }
            }
        }
        eventToCreate.setAttendees(attendees);
        ExternalAccount externalAccount = calendar.getExternalAccount();
        TokenResponse tokens = externalAccount.getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = externalAccount.getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        String email = externalAccount.getUserEmail();
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.empty());
        String calendarId = calendar.getExternalId();

        externalAccount.setToken(tokens);
        externalAccountRepository.save(externalAccount);
        try {
            int r = outlookService.updateEvent(eventId, eventToCreate).execute().code();
        } catch (IOException e) {
            throw new OutlookException(e.getMessage());
        }

    }

    public void deleteEvent(Calendar calendar, String eventId) {
        ExternalAccount externalAccount = calendar.getExternalAccount();
        TokenResponse tokens = externalAccount.getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = externalAccount.getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        String email = externalAccount.getUserEmail();
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.empty());
        String calendarId = calendar.getExternalId();

        externalAccount.setToken(tokens);
        externalAccountRepository.save(externalAccount);
        try {
            int r = outlookService.deleteEvent(eventId).execute().code();
        } catch (IOException e) {
            throw new OutlookException(e.getMessage());
        }

    }

    public String createEvent(Calendar calendar, EventDTOPost eventDTOPost) {
        OutlookEventMapper outlookEventMapper = Mappers.getMapper(OutlookEventMapper.class);
        //ObjectMapper objectMapper = new ObjectMapper();//no
        EventOutlookDTOPost eventToCreate = new EventOutlookDTOPost();
        eventToCreate = outlookEventMapper.eventToEventOutlook(eventDTOPost);
        Pattern pattern = new Pattern();
        Range range = new Range();
        if (eventDTOPost.getRecurrence() != null) {
            String[] strings = eventDTOPost.getRecurrence().split(";");
            for (String part : strings) {
                String pieces[] = part.split("=");
                switch (pieces[0]) {
                    case "FREQ":
                        pattern.setType(pieces[1]);
                        break;
                    case "INTERVAL":
                        pattern.setInterval(Integer.parseInt(pieces[1]));
                        break;
                    case "BYDAY": {
                        String days[] = pieces[1].split(",");
                        List<String> newDays = new ArrayList<>();
                        for (String day : days) {
                            switch (day) {
                                case "MO":
                                    newDays.add("monday");
                                    break;
                                case "TU":
                                    newDays.add("tuesday");
                                    break;
                                case "WE":
                                    newDays.add("wednesday");
                                    break;
                                case "TH":
                                    newDays.add("thursday");
                                    break;
                                case "FR":
                                    newDays.add("friday");
                                    break;
                                case "SA":
                                    newDays.add("saturday");
                                    break;
                                case "SU":
                                    newDays.add("sunday");
                                    break;
                            }
                        }
                        //String[] str=newDays.toArray(new String[] {});
                        pattern.setDaysOfWeek(newDays.toArray(new String[]{}));
                        break;
                    }
                    case "UNTIL": {
                        range.setType(RecurrenceRangeTypes.endDate.name());
                        DateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
                        Date date = null;
                        try {
                            date = format.parse(pieces[1]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        range.setEndDate(date);

                        break;
                    }
                    case "COUNT": {
                        range.setType(RecurrenceRangeTypes.numbered.name());
                        range.setNumberOfOccurrences(Integer.parseInt(pieces[1]));
                    }

                }
            }
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            try {
                Date date = format.parse(eventDTOPost.getDateTimeBegin().toLocalDate().toString());
                range.setStartDate(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // range.setStartDate(new Date(eventDTOPost.getDateTimeBegin()
            //       .toInstant(ZoneOffset.of(ZoneId.of(eventDTOPost.getTimeZone()))).toEpochMilli()));
            //java.sql.Timestamp.valueOf( eventDTOPost.getDateTimeBegin()));//.toInstant(ZoneOffset.UTC)));
            if (range.getType() == null)
                range.setType(RecurrenceRangeTypes.noEnd.name());
            Recurrence recurrence = new Recurrence();
            recurrence.setPattern(pattern);
            recurrence.setRange(range);
            eventToCreate.setRecurrence(recurrence);//objectMapper.readValue(eventDTOPost.getRecurrence(), Recurrence.class));
        }

        Attendee attendee = new Attendee();
        attendee.setType("required");
        EmailAddress emailAddress = new EmailAddress();
        emailAddress.setName(calendar.getExternalAccount().getUserName());
        emailAddress.setAddress(calendar.getExternalAccount().getUserEmail());
        attendee.setEmailAddress(emailAddress);
        List<Attendee> attendees = new ArrayList<>();
        //attendees.add(attendee);

        if (eventDTOPost.getMembersAccounts() != null) {

            for (EventAccountsDTOPost member : eventDTOPost.getMembersAccounts()) {
                log.info("{}", member.getExtSystem());
                log.info("{}", member.getLogin());
                log.info("{}", member.getName());
                if (member.getExtSystem() != null)
                    if (member.getExtSystem().equals(ExternalSystem.OUTLOOK.name())) {

                        Attendee attendee2 = new Attendee();
                        if (member.isImportant())
                            attendee2.setType("required");
                        else attendee2.setType("optional");
                        EmailAddress emailAddress2 = new EmailAddress();
                        emailAddress2.setName(member.getName());
                        emailAddress2.setAddress(member.getLogin());
                        attendee2.setEmailAddress(emailAddress2);
                        attendees.add(attendee2);
                    }
            }
        }
        eventToCreate.setAttendees(attendees);

        TokenResponse tokens = calendar.getExternalAccount().getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = calendar.getExternalAccount().getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        //externalAccount
        String email = calendar.getExternalAccount().getUserEmail();
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.<String>empty());

        calendar.getExternalAccount().setToken(tokens);
        externalAccountRepository.save(calendar.getExternalAccount());
        try {
            Map<String, Object> event = outlookService.createEvent(calendar.getExternalId(), eventToCreate).execute().body();
            if (event != null)
                return event.get("id").toString();

        } catch (IOException e) {
            throw new OutlookException(e.getMessage());
        }
        return null;
    }

    public List<CalendarDTOGet> getMyNotConnectedCalendars(ExternalAccount externalAccount) {
        TokenResponse tokens = externalAccount.getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = externalAccount.getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        String email = externalAccount.getUserEmail();
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.<String>empty());
        externalAccount.setToken(tokens);
        externalAccountRepository.save(externalAccount);
        try {
            PagedResult<CalendarDTOGet> calendars = outlookService.getCalendars().execute().body();
            if (calendars != null) {
                List<CalendarDTOGet> calendarDTOGets = new ArrayList<>();
                for (CalendarDTOGet cal : calendars.getValue())
                    if (cal.getOwner().getAddress().equals(externalAccount.getUserEmail()))
                        calendarDTOGets.add(cal);
                return calendarDTOGets;
            }

        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new OutlookException(e.getMessage());
        }
        return null;
    }

    public List<CalendarDTOGet> getMySharedCalendars(ExternalAccount externalAccount, String login) {
        TokenResponse tokens = externalAccount.getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = externalAccount.getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        String email = externalAccount.getUserEmail();
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.<String>empty());
        externalAccount.setToken(tokens);
        externalAccountRepository.save(externalAccount);
        try {
            PagedResult<CalendarDTOGet> calendars = outlookService.getCalendars().execute().body();
            if (calendars != null) {
                List<CalendarDTOGet> calendarDTOGets = new ArrayList<>();
                for (CalendarDTOGet cal : calendars.getValue())
                    if (cal.getOwner().getAddress().equals(login))
                        calendarDTOGets.add(cal);
                return calendarDTOGets;
            }

        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new OutlookException(e.getMessage());
        }
        return null;
    }

    public List<EventDTOGetNotMy> getUserEvents(String calendarId, LocalDateTime begin, LocalDateTime end,
                                                String timeZone, ExternalAccount myExtAcc) {
        TokenResponse tokens = myExtAcc.getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = myExtAcc.getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        String email = myExtAcc.getUserEmail();
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.of(timeZone));
        myExtAcc.setToken(tokens);
        externalAccountRepository.save(myExtAcc);
        try {
            PagedResult<EventDTOGetOutlook> events = outlookService.getMyCalendarEvents(calendarId, begin.toString(), end.toString()).execute().body();

            List<EventDTOGetNotMy> result = new ArrayList<>();
            if (events != null) {
                for (EventDTOGetOutlook event : events.getValue()) {
                    EventDTOGetNotMy newEvent = new EventDTOGetNotMy();
                    LocalDateTime startDateTime = LocalDateTime.parse(event.getStart().getDateTime());
                    LocalDateTime endDateTime = LocalDateTime.parse(event.getEnd().getDateTime());
                    newEvent.setDateTimeBegin(startDateTime.toString());
                    newEvent.setDateTimeEnd(endDateTime.toString());
                    result.add(newEvent);
                }
                return result;
            }
        } catch (IOException e) {
            throw new OutlookException(e.getMessage());
        }
        return null;

    }

    public List<EventDTOGetMy> getMyCalendarEvents(Calendar calendar, LocalDateTime begin, LocalDateTime end, String timeZone) {
        ExternalAccount externalAccount = calendar.getExternalAccount();
        TokenResponse tokens = externalAccount.getToken();
        if (tokens == null) {
            throw new OutlookException("not signed in user");
        }
        String tenantId = externalAccount.getTenantId();
        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        String email = externalAccount.getUserEmail();
        log.info("TimeZone{}: ", timeZone);
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email,
                Optional.of(timeZone));
        String calendarId = calendar.getExternalId();

        externalAccount.setToken(tokens);
        externalAccountRepository.save(externalAccount);
        try {
            PagedResult<EventDTOGetOutlook> events = outlookService.getMyCalendarEvents(calendarId, begin.toString(), end.toString()).execute().body();
            List<EventDTOGetMy> result = new ArrayList<>();
            if (events != null) {
                for (EventDTOGetOutlook event : events.getValue()) {
                    EventDTOGetMy newEvent = new EventDTOGetMy();
                    newEvent.setName(event.getSubject());
                    newEvent.setExt_id(event.getId());
                    LocalDateTime startDateTime = LocalDateTime.parse(event.getStart().getDateTime());
                    LocalDateTime endDateTime = LocalDateTime.parse(event.getEnd().getDateTime());
                    // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-ddTHH:mm:ss.SSSSSSS");
                    // LocalDateTime dateTime = LocalDateTime.parse(event.getStart().getDateTime(), formatter);
                    newEvent.setDateTimeBegin(startDateTime.toString());//LocalDateTime.parse(event.getStart().getDateTime(), formatter));
                    newEvent.setDateTimeEnd(endDateTime.toString());//LocalDateTime.parse(event.getEnd().getDateTime(), formatter));
                    String response = event.getResponseStatus().getResponse();
                    switch (response) {
                        case "none":
                            newEvent.setStatus(EventMembersStatus.none);
                            break;
                        case "organizer":
                            newEvent.setStatus(EventMembersStatus.creator);
                            break;
                        case "accepted":
                            newEvent.setStatus(EventMembersStatus.accepted);
                            break;
                        case "declined":
                            newEvent.setStatus(EventMembersStatus.declined);
                            break;
                        case "notResponded":
                            newEvent.setStatus(EventMembersStatus.none);
                            break;
                        case "tentativelyAccepted":
                            newEvent.setStatus(EventMembersStatus.accepted);
                            break;
                    }
                    //newEvent.setStatus(event.getResponseStatus().getResponse());
                    if (!newEvent.getStatus().equals(EventMembersStatus.declined))
                        result.add(newEvent);
                }
                return result;
            }
        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new OutlookException(e.getMessage());
        }
        return null;

    }


}
