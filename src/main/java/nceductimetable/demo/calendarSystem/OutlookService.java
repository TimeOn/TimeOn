package nceductimetable.demo.calendarSystem;

import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.dtoOutlook.EventDTOGetOutlook;
import nceductimetable.demo.dtoOutlook.EventOutlookDTOPost;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface OutlookService {

    @GET("/v1.0/me")
    Call<Map<String, Object>> getCurrentUser();

    @GET("/v1.0/me/calendars")
    Call<PagedResult<CalendarDTOGet>> getCalendars();

    @POST("/v1.0/me/calendars/{id}/events")
    Call<Map<String, Object>> createEvent(@Path("id") String id,
            @Body() EventOutlookDTOPost event);

    @GET("/v1.0/me/events/{id}")
    Call<MyEventJson> getEvent(
            @Path("id") String id,
            //@Query("$orderby") String orderBy,
            @Query("$select") String select
            //@Query("$top") Integer maxResults
    );

    @GET("/v1.0/me/calendars/{id}/calendarView")
    Call<PagedResult<EventDTOGetOutlook>> getMyCalendarEvents(@Path("id") String id,
                                                              @Query("startDateTime") String startDateTime,
                                                              @Query("endDateTime") String endDateTime);

    @GET("/v1.0/users/{userPrincipalName}/calendar/calendarView")
    Call <PagedResult<EventDTOGetOutlook>> getUserEvents(@Path("userPrincipalName") String userPrincipalName,
                                                         @Query("startDateTime") String startDateTime,
                                                         @Query("endDateTime") String endDateTime);

    @DELETE("/v1.0/me/events/{id}")
    Call <ResponseStatus> deleteEvent(@Path("id") String id);

    @PATCH("/v1.0/me/events/{id}")
    Call <ResponseStatus> updateEvent(@Path("id") String id,
                                      @Body() EventOutlookDTOPost event);

}
