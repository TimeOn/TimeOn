package nceductimetable.demo.calendarSystem;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.*;
import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.dtoGet.EventDTOGetMy;
import nceductimetable.demo.dtoGet.EventDTOGetNotMy;
import nceductimetable.demo.dtoOutlook.EmailAddress;
import nceductimetable.demo.dtoOutlook.EventOutlookDTOPost;
import nceductimetable.demo.dtoPost.EventAccountsDTOPost;
import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.enumForEvents.EventMembersStatus;
import nceductimetable.demo.exceptions.GoogleExeption;
import nceductimetable.demo.model.Calendar;
import nceductimetable.demo.model.ExternalAccount;
import nceductimetable.demo.repositories.ExternalAccountRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
public class GoogleCalendar implements CalendarSystem {

    private ExternalAccountRepository externalAccountRepository;
    public GoogleCalendar(ExternalAccountRepository externalAccountRepository){
        this.externalAccountRepository=externalAccountRepository;
    }
    private final static Log logger = LogFactory.getLog(GoogleCalendar.class);
    private static final String APPLICATION_NAME = "";
    private static HttpTransport httpTransport;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static com.google.api.services.calendar.Calendar client;

    static GoogleClientSecrets clientSecrets;
    static GoogleAuthorizationCodeFlow flow;
    static Credential credential;

    private static String clientId;
    private static String clientSecret;
    private static  String redirectURI;


    private static void loadConfig() throws IOException {

        String authConfigFile = "auth.properties";
        InputStream authConfigStream = GoogleCalendar.class.getClassLoader().getResourceAsStream(authConfigFile);

        if (authConfigStream != null) {
            Properties authProps = new Properties();
            try {
                authProps.load(authConfigStream);
                clientId = authProps.getProperty("google.client.client-id");
                clientSecret = authProps.getProperty("google.client.client-secret");
                redirectURI = authProps.getProperty("google.client.redirectUri");
            } finally {
                authConfigStream.close();
            }
        }
        else {
            throw new FileNotFoundException("Property file '" + authConfigFile + "' not found in the classpath.");
        }
    }

    public static String authorize(String timeonToken) throws Exception {
        loadConfig();
        redirectURI+= "/" + timeonToken;
        AuthorizationCodeRequestUrl authorizationUrl;
        if (flow == null) {
            GoogleClientSecrets.Details web = new GoogleClientSecrets.Details();
            web.setClientId(clientId);
            web.setClientSecret(clientSecret);
            clientSecrets = new GoogleClientSecrets().setWeb(web);
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
                    Collections.singleton(CalendarScopes.CALENDAR)).build();
        }
        authorizationUrl = flow.newAuthorizationUrl().setRedirectUri(redirectURI);
        System.out.println("cal authorizationUrl->" + authorizationUrl);

        return authorizationUrl.build();
    }


    public static TokenResponse getTokenFromLoginCode(String code){
        try {
            TokenResponse response = flow.newTokenRequest(code).setRedirectUri(redirectURI).execute();
            return(response);
        } catch (Exception e) {
            logger.warn("Exception while handling OAuth2 callback (" + e.getMessage() + ")."
                    + " Redirecting to google connection status page.");
        }
        return null;
    }

    public static com.google.api.services.calendar.Calendar getServiceFromToken(TokenResponse token) throws IOException{
        credential = flow.createAndStoreCredential(token, "userID");
        return  client = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME).build();
    }

    //TO DO получить имена пользователей и тп сейчас все забито емейлами
    public List<CalendarDTOGet> getMyNotConnectedCalendars(ExternalAccount externalAccount){
        if (externalAccount.getAccessToken() == null) {
            throw new GoogleExeption("not signed in user");
        }
        com.google.api.client.auth.oauth2.TokenResponse tokenResponse = new com.google.api.client.auth.oauth2.TokenResponse();
        tokenResponse.setAccessToken(externalAccount.getAccessToken());
        tokenResponse.setRefreshToken(externalAccount.getRefreshToken());
        tokenResponse.setTokenType(externalAccount.getRefreshToken());
        List<CalendarDTOGet> externalCalendars = new ArrayList<>();
        try {
            client = GoogleCalendar.getServiceFromToken(tokenResponse);
            String pageToken = null;
            do {
                CalendarList calendarList = client.calendarList().list().setPageToken(pageToken).execute();
                List<CalendarListEntry> items = calendarList.getItems();

                for (CalendarListEntry calendarListEntry : items) {
                    if(calendarListEntry.getAccessRole().toString().equals("owner") ){
                        CalendarDTOGet calendarDTOGet = new CalendarDTOGet();
                        EmailAddress email = new EmailAddress();
                        email.setAddress(externalAccount.getUserEmail());
                        email.setName(externalAccount.getUserEmail());
                        calendarDTOGet.setOwner(email);
                        calendarDTOGet.setColor(calendarListEntry.getColorId());
                        calendarDTOGet.setName(calendarListEntry.getSummary());
                        calendarDTOGet.setId(calendarListEntry.getId());
                        externalCalendars.add(calendarDTOGet);
                    }
                }
                pageToken = calendarList.getNextPageToken();
            } while (pageToken != null);
            return externalCalendars;
        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new GoogleExeption(e.getMessage());
        }
    }


    public String createEvent(Calendar calendar, EventDTOPost eventDTOPost){
        ExternalAccount externalAccount=calendar.getExternalAccount();
        if (externalAccount.getAccessToken() == null) {
            throw new GoogleExeption("not signed in user");
        }
        com.google.api.client.auth.oauth2.TokenResponse tokenResponse = new com.google.api.client.auth.oauth2.TokenResponse();
        tokenResponse.setAccessToken(externalAccount.getAccessToken());
        tokenResponse.setRefreshToken(externalAccount.getRefreshToken());
        tokenResponse.setTokenType(externalAccount.getRefreshToken());
        try {
            client = GoogleCalendar.getServiceFromToken(tokenResponse);

            Event event = new Event()
                    .setSummary(eventDTOPost.getName())
                    .setLocation(eventDTOPost.getAddress())
                    .setDescription(eventDTOPost.getDescription());

            DateTime startDateTime = new DateTime(fromLDTtoString(eventDTOPost.getDateTimeBegin())+displayTimeZone(eventDTOPost.getTimeZone()));
            EventDateTime start = new EventDateTime()
                    .setDateTime(startDateTime)
                    .setTimeZone(eventDTOPost.getTimeZone());
            event.setStart(start);

            DateTime endDateTime = new DateTime(fromLDTtoString(eventDTOPost.getDateTimeEnd())+displayTimeZone(eventDTOPost.getTimeZone()));
            EventDateTime end = new EventDateTime()
                    .setDateTime(endDateTime)
                    .setTimeZone(eventDTOPost.getTimeZone());
            event.setEnd(end);

            if(eventDTOPost.getRecurrence() != null){
                String[] recurrence = new String[] {eventDTOPost.getRecurrence()};
                event.setRecurrence(Arrays.asList(recurrence));
            }

           /* if(eventDTOPost.getMembersAccounts() != null){
                List<EventAttendee> attendees = new ArrayList<>();
                List<EventAccountsDTOPost> membersAccounts =  eventDTOPost.getMembersAccounts();
                for ( EventAccountsDTOPost eventAccountsDTOPost: membersAccounts) {
                    attendees.add(new EventAttendee().setEmail( eventAccountsDTOPost.getLogin()));
                }
                event.setAttendees(attendees);
            }*/


            EventReminder[] reminderOverrides = new EventReminder[] {
                    new EventReminder().setMethod("email").setMinutes(24 * 60),
                    new EventReminder().setMethod("popup").setMinutes(10),
            };
            Event.Reminders reminders = new Event.Reminders()
                    .setUseDefault(false)
                    .setOverrides(Arrays.asList(reminderOverrides));
            event.setReminders(reminders);
            //-----Календари
            String calendarId = calendar.getExternalId();
            event = client.events().insert(calendarId, event).execute();
            System.out.printf("Event created: %s\n", event.getHtmlLink());

            return  event.getId().toString();
        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new GoogleExeption(e.getMessage());
        }
    };

    public List<EventDTOGetNotMy> getUserEvents(String login, LocalDateTime begin, LocalDateTime end,
                                                String timeZone, ExternalAccount my){
        if (my.getAccessToken() == null) {
            throw new GoogleExeption("not signed in user");
        }
        com.google.api.client.auth.oauth2.TokenResponse tokenResponse = new com.google.api.client.auth.oauth2.TokenResponse();
        tokenResponse.setAccessToken(my.getAccessToken());
        tokenResponse.setRefreshToken(my.getRefreshToken());
        tokenResponse.setTokenType(my.getRefreshToken());
        try {
            List<EventDTOGetNotMy> result=new ArrayList<>();
            client = GoogleCalendar.getServiceFromToken(tokenResponse);
            String pageToken = null;
            do {
                DateTime startDateTime = new DateTime(fromLDTtoString(begin) + displayTimeZone(timeZone));
                DateTime endDateTime = new DateTime(fromLDTtoString(end) + displayTimeZone(timeZone));

                com.google.api.services.calendar.model.Events events = client
                        .events()
                        .list(login)
                        .setPageToken(pageToken)
                        .setTimeMin(startDateTime)
                        .setTimeMax(endDateTime)
                        .execute();
                List<Event> items = events.getItems();
                if(items.isEmpty()){

                    List<EventDTOGetNotMy> res =new ArrayList<>();
                    return res;
                }


                for (Event event : items) {
                    if(event.getStart().toString().contains("dateTime")) {
                        EventDTOGetNotMy eventDTOGetNotMy = new EventDTOGetNotMy();
                        eventDTOGetNotMy.setDateTimeBegin(fromDateTimeToString(event.getStart().getDateTime()));
                        eventDTOGetNotMy.setDateTimeEnd(fromDateTimeToString(event.getEnd().getDateTime()));
                        result.add(eventDTOGetNotMy);

                    }
                }
                pageToken = events.getNextPageToken();
            } while (pageToken != null);
            return result;
        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new GoogleExeption(e.getMessage());
        }
    }

    public List<EventDTOGetMy> getMyCalendarEvents(Calendar calendar, LocalDateTime begin, LocalDateTime end, String timeZone){
        ExternalAccount externalAccount=calendar.getExternalAccount();
        if (externalAccount.getAccessToken() == null) {
            throw new GoogleExeption("not signed in user");
        }
        String calendarId = calendar.getExternalId();
        com.google.api.client.auth.oauth2.TokenResponse tokenResponse = new com.google.api.client.auth.oauth2.TokenResponse();
        tokenResponse.setAccessToken(externalAccount.getAccessToken());
        tokenResponse.setRefreshToken(externalAccount.getRefreshToken());
        tokenResponse.setTokenType(externalAccount.getRefreshToken());
        try {
            List<EventDTOGetMy> result=new ArrayList<>();
            client = GoogleCalendar.getServiceFromToken(tokenResponse);
            String pageToken = null;
            do {
                DateTime startDateTime = new DateTime(fromLDTtoString(begin) + displayTimeZone(timeZone));
                DateTime endDateTime = new DateTime(fromLDTtoString(end) + displayTimeZone(timeZone));

                com.google.api.services.calendar.model.Events events = client
                        .events()
                        .list(calendarId)
                        .setPageToken(pageToken)
                        .setTimeMin(startDateTime)
                        .setTimeMax(endDateTime)
                        .execute();
                List<Event> items = events.getItems();
                if(items.isEmpty()){
                    List<EventDTOGetMy> res=new ArrayList<>();
                    return res;
                }


                for (Event event : items) {
                    if(event.getStart().toString().contains("dateTime")) {
                        EventDTOGetMy eventDTOGetMy = new EventDTOGetMy();
                        eventDTOGetMy.setExt_id(event.getId());
                        eventDTOGetMy.setName(event.getSummary());
                        eventDTOGetMy.setDateTimeBegin(fromDateTimeToString(event.getStart().getDateTime()));
                        eventDTOGetMy.setDateTimeEnd(fromDateTimeToString(event.getEnd().getDateTime()));
                        eventDTOGetMy.setStatus(returnStatusFromGoogle(event.getStatus()));
                        result.add(eventDTOGetMy);
                    }

                }
                pageToken = events.getNextPageToken();
            } while (pageToken != null);
            return result;
        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new GoogleExeption(e.getMessage());
        }

    }

    private static EventMembersStatus returnStatusFromGoogle(String status){
        switch(status){
            case "confirmed" :
                return EventMembersStatus.accepted;
            case "tentative" :
                return EventMembersStatus.none;
            case "cancelled" :
                return EventMembersStatus.declined;
            default:
                return EventMembersStatus.creator;
        }
    }

    private static String fromLDTtoString(LocalDateTime time){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        String formatDateTime = time.format(formatter);
        return(formatDateTime);
    }
    private static String fromDateTimeToString(DateTime date){
        String rightFormat = date.toString().substring(0,date.toString().length() - 13);
        return rightFormat;
    }

    public List<CalendarDTOGet> getMySharedCalendars(ExternalAccount externalAccount, String login){
        if (externalAccount.getAccessToken() == null) {
            throw new GoogleExeption("not signed in user");
        }
        com.google.api.client.auth.oauth2.TokenResponse tokenResponse = new com.google.api.client.auth.oauth2.TokenResponse();
        tokenResponse.setAccessToken(externalAccount.getAccessToken());
        tokenResponse.setRefreshToken(externalAccount.getRefreshToken());
        tokenResponse.setTokenType(externalAccount.getRefreshToken());
        List<CalendarDTOGet> externalCalendars = new ArrayList<>();
        try {
            client = GoogleCalendar.getServiceFromToken(tokenResponse);
            String pageToken = null;
            do {
                CalendarList calendarList = client.calendarList().list().setPageToken(pageToken).execute();
                List<CalendarListEntry> items = calendarList.getItems();

                for (CalendarListEntry calendarListEntry : items) {
                    if(!calendarListEntry.getAccessRole().toString().equals("owner") ){
                        CalendarDTOGet calendarDTOGet = new CalendarDTOGet();
                        EmailAddress email = new EmailAddress();
                        email.setAddress(externalAccount.getUserEmail());
                        email.setName(externalAccount.getUserEmail());
                        calendarDTOGet.setOwner(email);
                        calendarDTOGet.setColor(calendarListEntry.getColorId());
                        calendarDTOGet.setName(calendarListEntry.getSummary());
                        calendarDTOGet.setId(calendarListEntry.getId());
                        externalCalendars.add(calendarDTOGet);
                    }
                }
                pageToken = calendarList.getNextPageToken();
            } while (pageToken != null);
            return externalCalendars;
        } catch (IOException e) {
            log.error("{}", e.getMessage(), e);
            throw new GoogleExeption(e.getMessage());
        }
    };
    private static String displayTimeZone(String TimeZoneID) {
        TimeZone  tz = TimeZone.getTimeZone(TimeZoneID);
        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours >= 0) {
            result = String.format("+%02d:%02d", hours, minutes);
        } else {
            hours*=-1;
            result = String.format("-%02d:%02d", hours, minutes);
        }
        return result;

    }
    public void updateEvent(Calendar calendar, EventDTOPost eventDTOPost, String eventId){}
    public void deleteEvent(Calendar calendar, String eventId){
    }
    public MyEventJson getEventById(Calendar calendar, String eventId, String timeZone){return null;};

}
