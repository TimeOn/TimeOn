package nceductimetable.demo.calendarSystem;

import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.dtoGet.EventDTOGetMy;
import nceductimetable.demo.dtoGet.EventDTOGetNotMy;
import nceductimetable.demo.dtoOutlook.EventOutlookDTOPost;
import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.model.Calendar;
import nceductimetable.demo.model.ExternalAccount;

import java.time.LocalDateTime;
import java.util.List;

public interface CalendarSystem {
    public String createEvent(Calendar calendar, EventDTOPost eventDTOPost);
    public List<CalendarDTOGet> getMyNotConnectedCalendars(ExternalAccount externalAccount);
    public List<CalendarDTOGet> getMySharedCalendars(ExternalAccount externalAccount, String login);
    public List<EventDTOGetNotMy> getUserEvents(String calendarId, LocalDateTime begin, LocalDateTime end,
                                                String timeZone, ExternalAccount my);
    public List<EventDTOGetMy> getMyCalendarEvents(Calendar calendar, LocalDateTime begin, LocalDateTime end, String timeZone);
    public void updateEvent(Calendar calendar, EventDTOPost eventDTOPost, String eventId);
    public void deleteEvent(Calendar calendar, String eventId);
    public MyEventJson getEventById(Calendar calendar, String eventId, String timeZone);

}
