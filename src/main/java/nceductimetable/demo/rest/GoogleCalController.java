package nceductimetable.demo.rest;

import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.calendarSystem.GoogleCalendar;
import nceductimetable.demo.dtoGet.ExternalAccountDTOGet;
import nceductimetable.demo.services.AccountService;
import nceductimetable.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
public class GoogleCalController {

    private final AccountService accountService;
    private final UserService userService;

    @Autowired
    public GoogleCalController(AccountService accountService,
                               UserService userService) {
        this.accountService = accountService;
        this.userService = userService;
    }
    private int accountId;

    @RequestMapping(value = "/google/{timeonToken:.+}", method = RequestMethod.GET)
    public RedirectView googleConnectionStatus(HttpServletRequest request,
                                               @PathVariable("timeonToken") String token) throws Exception {
        return new RedirectView(GoogleCalendar.authorize(token));
    }

/*    @RequestMapping(value = "/login/google/{timeonToken:.+}", method = RequestMethod.GET, params = "code")
    public ResponseEntity oauth2Callback(@RequestParam(value = "code") String code,
                                         @PathVariable String timeonToken)
    {
        accountId = userService.getMyId(timeonToken);
        return ResponseEntity.ok(accountService.googleRegistrateExternalAccount(accountId, code));
    }*/

    @RequestMapping(value = "/login/google/{timeonToken:.+}", method = RequestMethod.GET, params = "code")
    public RedirectView oauth2Callback(@RequestParam(value = "code") String code,
                                       @PathVariable String timeonToken)
    {
        accountId = userService.getMyId(timeonToken);
        accountService.googleRegistrateExternalAccount(accountId, code);
        return new RedirectView("http://localhost:8081/pages/examples/profile.html");
    }

    @RequestMapping(value="/me/googleExternalAccounts", method=RequestMethod.GET)
    public ExternalAccountDTOGet getMyExternalAccounts(@RequestHeader("Auth-Token") String token){
        return accountService.getMyExternalAccounts(userService.getMyId(token)).get(0);
    }


}
