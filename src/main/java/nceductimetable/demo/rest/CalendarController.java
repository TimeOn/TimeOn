package nceductimetable.demo.rest;

import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.services.CalendarIntService;
import nceductimetable.demo.services.CalendarService;
import nceductimetable.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CalendarController {
    private final UserService userService;
    private final CalendarService calendarService;
    private final CalendarIntService calendarIntService;

    @Autowired
    public CalendarController(UserService userService,
                              CalendarService calendarService,
                              CalendarIntService calendarIntService){
        this.userService=userService;
        this.calendarService=calendarService;
        this.calendarIntService=calendarIntService;
    }



    @RequestMapping(value="/my/calendars", method= RequestMethod.GET)
    public List<CalendarDTOGet> getMyCalendars(@RequestHeader("Auth-Token") String token){
        return calendarService.getMyCalendars( userService.getMyId(token));
    }

    @RequestMapping(value="/get/myNotConnectedCalendars/{ext_accountId}", method=RequestMethod.GET)
    public List<CalendarDTOGet> getMyNotConnectedCalendars(@RequestHeader("Auth-Token") String token,
                                                           @PathVariable int ext_accountId){
        return calendarIntService.getMyNotConnectedCalendars(ext_accountId);
    }



    @RequestMapping(value="/post/calendars", method=RequestMethod.POST)
    public ResponseEntity createCalendars(@RequestHeader("Auth-Token") String token,
                                          @RequestBody List<CalendarDTOGet> calendars){
        calendarService.createCalendars(userService.getMyId(token), calendars);
            return ResponseEntity.ok().build();
        //return ResponseEntity.badRequest().build();
    }
}
