package nceductimetable.demo.rest;

import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.calendarSystem.oauth.AuthHelper;
import nceductimetable.demo.dto.AccountForSearch;
import nceductimetable.demo.dto.AccountReturnedById;
import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.dtoGet.ExternalAccountDTOGet;
import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.model.*;
import nceductimetable.demo.repositories.UserRepository;
import nceductimetable.demo.services.*;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.UUID;


import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class AccountController {
    private final AccountService accountService;
    private final EventService eventService;
    //private final TokenHandler tokenHandler;
    // private final UserRepository userRepository;
    private final CalendarService calendarService;
    private final UserService userService;
    private UUID exp_state, exp_nonce;
    private int accountId;


    @Autowired
    public AccountController(AccountService accountService, EventService eventService,
                             UserService userService,
                             //TokenHandler tokenHandler,
                             // UserRepository userRepository,
                             CalendarService calendarService) {
        this.accountService = accountService;
        this.eventService = eventService;
        this.userService = userService;
        //this.tokenHandler = tokenHandler;
        //this.userRepository = userRepository;
        this.calendarService = calendarService;
    }


    @RequestMapping(value = "/registration/outlook/getUrl", method = RequestMethod.GET)
    public String getRegistationURLOutlook(@RequestHeader("Auth-Token") String token) {
        exp_state = UUID.randomUUID();
        exp_nonce = UUID.randomUUID();
        accountId=userService.getMyId(token);
        return AuthHelper.getLoginUrl(exp_state, exp_nonce);

    }

    @RequestMapping(value = "/registration/outlook/returnResponse", method = RequestMethod.POST)
    public ResponseEntity responseFromOutlook(//@RequestHeader("Auth-Token") String token,
                                              @RequestParam("code") String code,
                                              @RequestParam("id_token") String idToken,
                                              @RequestParam("state") UUID state) {
        if (state.equals(exp_state)) {
            return accountService.registrateExternalAccount(accountId,
                    code, idToken, exp_nonce.toString());
        }
        return ResponseEntity.notFound().build();

    }

    @RequestMapping(value = "/me/id", method = RequestMethod.GET)
    public Integer getMyId(@RequestHeader("Auth-Token") String token) {
        return userService.getMyId(token);
    }

    @RequestMapping(value = "/search/{searchRequest}", method = RequestMethod.GET)
    public List<AccountForSearch> searchAccounts(@RequestHeader("Auth-Token") String token,
                                                 @PathVariable String searchRequest, int countOfRecords, int pageNumber) {
        return accountService.searchAccounts(searchRequest, countOfRecords, pageNumber);
    }

    @RequestMapping(value="/me/externalAccounts", method=RequestMethod.GET)
    public List<ExternalAccountDTOGet> getMyExternalAccounts(@RequestHeader("Auth-Token") String token){
        return accountService.getMyExternalAccounts(userService.getMyId(token));
    }

    @RequestMapping(value="/user/{id}", method=RequestMethod.GET)
    public AccountReturnedById getUserById(@RequestHeader("Auth-Token") String token,
                                           @PathVariable int id){
        return accountService.getUserById(userService.getMyId(token), id);
    }


}

