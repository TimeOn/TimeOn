package nceductimetable.demo.rest;

import nceductimetable.demo.dto.AccountsEmployments;
import nceductimetable.demo.dto.TimePicking;
import nceductimetable.demo.dto.TimePickingIn;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.services.EventService;
import nceductimetable.demo.services.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class TimeController {

    private final TimeService timeService;

    @Autowired
    public TimeController(TimeService timeService) {
        this.timeService = timeService;
    }

    @RequestMapping(value = "/timeMeeting", method = RequestMethod.POST)
    public ResponseEntity meetingInDay(@RequestBody TimePickingIn timePicking) {
        return ResponseEntity.ok(timeService.calendar(timePicking.getBegin(), timePicking.getEnd(), timePicking.getAccounts()));
        //return timeService.meetingTime(timePicking);
    }

    @RequestMapping(value = "/employment", method = RequestMethod.POST)
    public ResponseEntity employment(@RequestBody AccountsEmployments accountsEmployments) {
        return ResponseEntity.ok(timeService.employmentnew(accountsEmployments.getAccounts(), accountsEmployments.getBegin(), accountsEmployments.getEnd()));
    }

    /*@RequestMapping(value = "/calendarTime", method = RequestMethod.POST)
    public ResponseEntity calendarTime(@RequestBody TimePickingIn timePicking) {
        return ResponseEntity.ok(timeService.calendarTime(timePicking.getBegin(), timePicking.getEnd(), timePicking.getAccounts()));
    }*/

    @RequestMapping(value = "/pickTime", method = RequestMethod.POST)
    public ResponseEntity pickTime(@RequestBody TimePickingIn timePicking, Integer time) {
        return ResponseEntity.ok(timeService.pickTime(time ,timePicking.getBegin(), timePicking.getEnd(), timePicking.getAccounts()));
    }
}
