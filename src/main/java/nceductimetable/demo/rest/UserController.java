package nceductimetable.demo.rest;

import com.google.common.hash.Hashing;
import nceductimetable.demo.dto.AccountLogin;
import nceductimetable.demo.dto.AccountRegister;
import nceductimetable.demo.services.TokenHandler;
import nceductimetable.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

@RestController
public class UserController {

    @Value("${security.salt}")
    private String salt;

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final TokenHandler tokenHandler;

    @Autowired
    public UserController(UserService userService, TokenHandler tokenHandler, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.tokenHandler = tokenHandler;
        this.authenticationManager = authenticationManager;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity createUser(@RequestBody AccountRegister accountRegister) {
        userService.createUser(accountRegister);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/activate/{code}", method = RequestMethod.GET)
    public ResponseEntity activation(@PathVariable String code) {
        userService.activateUser(code);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity authentificate(@RequestBody AccountLogin accountLogin) {
        String hashed = Hashing.sha512().hashString(accountLogin.getPassword() + salt, StandardCharsets.UTF_8).toString();
        //authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(accountLogin.getLogin(), hashed));
        return ResponseEntity.ok(userService.loginUser(accountLogin));
    }

    @RequestMapping(value = "/parseToken", method = RequestMethod.GET)
    public ResponseEntity<?> parseAuthenticationToken(String token) throws AuthenticationException {
        Optional<String> login = tokenHandler.extractUserLogin(token);
        return ResponseEntity.ok(login.orElseThrow(() -> new UsernameNotFoundException("user was not found!")));
    }
}