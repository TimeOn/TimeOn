package nceductimetable.demo.rest;

import nceductimetable.demo.dto.AccountDTO;
import nceductimetable.demo.services.AccountSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountSettingsController {

    private final AccountSettingsService accountSettingsService;

    @Autowired
    public AccountSettingsController(AccountSettingsService accountSettingsService) {
        this.accountSettingsService = accountSettingsService;
    }

    @RequestMapping(value = "/account/{accountId}", method = RequestMethod.PUT)
    public ResponseEntity updateAccount(@PathVariable int accountId, @RequestBody AccountDTO newAccountData) {
        accountSettingsService.updateAccountData(accountId, newAccountData);
        return ResponseEntity.ok().build();
    }
}
