package nceductimetable.demo.rest;

import nceductimetable.demo.dto.AccountIsBusy;
import nceductimetable.demo.dtoGet.EventDTOGet;
import nceductimetable.demo.dtoGet.EventDTOGetMy;
import nceductimetable.demo.dtoGet.EventDTOGetNotMy;
import nceductimetable.demo.dtoGet.EventDTOWithAnswers;
import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.enumForEvents.EventMembersStatus;
import nceductimetable.demo.services.EventService;
import nceductimetable.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class EventController {
    private final EventService eventService;
    private final UserService userService;

    @Autowired
    public EventController(EventService eventService,
                           UserService userService) {
        this.eventService = eventService;
        this.userService=userService;
    }

    @RequestMapping(value = "/event", method = RequestMethod.POST)
    public ResponseEntity createEvent(@RequestHeader("Auth-Token") String token,
                                      @RequestBody EventDTOPost eventDTO
                                      //@RequestParam("timeZone") String timeZone
                                      ) {
        if (eventService.createEvent(userService.getMyId(token),eventDTO/* timeZone*/) > 0)
            return ResponseEntity.ok().build();
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value="/event/update", method = RequestMethod.PUT)
    public ResponseEntity updateEvent(@RequestHeader("Auth-Token") String token,
                                      @RequestBody EventDTOPost eventDTOPost,
                                      @RequestParam("id") int id){
        return eventService.updateEvent(userService.getMyId(token), eventDTOPost, id);
    }

    @RequestMapping(value="/event/cancel", method = RequestMethod.PUT)
    public ResponseEntity cancelEvent(@RequestHeader("Auth-Token") String token,
                                      @RequestParam("id") int id,
                                      @RequestParam("extId") String extId,
                                      @RequestParam ("calendarId") int calendarId){
        return eventService.cancelEvent(userService.getMyId(token), id, extId, calendarId);
    }


    @RequestMapping(value="/me/{eventId}", method=RequestMethod.GET)
    public EventDTOGet getEvent(@RequestHeader("Auth-Token") String token,
                                @PathVariable int eventId,
                                @RequestParam("extEventId") String extEventId,
                                @RequestParam ("calendarId") int calendarId,
                                @RequestParam("timeZone") String timeZone){
        return eventService.getEvent(userService.getMyId(token), eventId, extEventId, calendarId, timeZone);
    }



    @RequestMapping(value="/me/events", method=RequestMethod.GET)
    public List<EventDTOGetMy> getMyEvents(@RequestHeader("Auth-Token") String token,
                                           @RequestParam("fromDate")
                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                   LocalDateTime fromDate,
                                           @RequestParam("toDate")
                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                   LocalDateTime toDate,
                                           @RequestParam("timeZone") String timeZone,
                                           @RequestParam("calendarIds") List<Integer> calendarIds
    ){
        return eventService.getMyCalendarEvents( fromDate, toDate, timeZone, calendarIds);
    }

    @RequestMapping(value="/user/{login}/events", method=RequestMethod.GET)
    public List<EventDTOGetNotMy> getOthersEvents(@RequestHeader("Auth-Token") String token,
                                                  @RequestParam("fromDate")
                                                  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                          LocalDateTime fromDate,
                                                  @RequestParam("toDate")
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                              LocalDateTime toDate,
                                                  @RequestParam("timeZone") String timeZone,
                                                  @PathVariable(value="login") String login
                                                 // @RequestParam("externalSystem") String externalSystem
                                                  ){
        return eventService.getOthersEvents(login, fromDate, toDate, timeZone, userService.getMyId(token));
    }

    @RequestMapping(value="/me/getEventsOnStatus/{status}", method=RequestMethod.GET)
    public List<EventDTOGetMy> getEventsOnStatus(@RequestHeader("Auth-Token") String token,
                                                      @RequestParam("timeZone") String timeZone,
                                                      @PathVariable EventMembersStatus status,
                                                      int countOfRecords, int pageNumber){
        return eventService.getEventsOnStatus(userService.getMyId(token), timeZone,status, countOfRecords, pageNumber);
    }

    @RequestMapping(value="/me/{eventId}/changeEventStatus", method=RequestMethod.POST)
    public ResponseEntity changeEventStatus(@RequestHeader("Auth-Token") String token,
                                            @RequestParam("status") EventMembersStatus status,
                                            @PathVariable int eventId){
        return eventService.changeEventStatus(userService.getMyId(token), status, eventId );
    }

    @RequestMapping(value = "/me/getEventsWithAnswers", method = RequestMethod.GET)
    public List<EventDTOWithAnswers> getEventsWithAnswers(@RequestHeader("Auth-Token") String token,
                                                          @RequestParam("timeZone") String timeZone)
    {
        return eventService.getEventsWithAnswers(userService.getMyId(token), timeZone);
    }

    @RequestMapping(value="/user/{login}/isBusy", method=RequestMethod.GET)
    public AccountIsBusy getAccountBusy(@RequestHeader("Auth-Token") String token,
                                        @RequestParam("fromDate")
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                LocalDateTime fromDate,
                                        @RequestParam("toDate")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                    LocalDateTime toDate,
                                        @RequestParam("timeZone") String timeZone,
                                        @PathVariable(value="login") String login){
        return eventService.getAccountBusy(login, fromDate, toDate, timeZone, userService.getMyId(token));
    }




}
