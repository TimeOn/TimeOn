package nceductimetable.demo.rest;

import nceductimetable.demo.exceptions.ActivateAccountException;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.repositories.UserRepository;
import nceductimetable.demo.services.Mail;
import nceductimetable.demo.services.TokenHandler;
import nceductimetable.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {


    private final Mail letter;
    private final UserService userService;
    private final TokenHandler tokenHandler;
    private final UserRepository userRepository;

    @Autowired
    public TestController(Mail letter, UserService userService, TokenHandler tokenHandler, UserRepository userRepository) {
        this.letter = letter;
        this.userService = userService;
        this.tokenHandler = tokenHandler;
        this.userRepository = userRepository;

    }

    @RequestMapping(value = "/test/{name}", method = RequestMethod.GET)
    public String test(@PathVariable String name) {
        return name;
    }

    @RequestMapping(value = "/mail/{address}", method = RequestMethod.GET)
    public void testMail(@PathVariable String address, String subject, String text) {
        letter.sendLetter(address + ".ru", subject, text);
    }

    @RequestMapping(value = "/tokenTest", method = RequestMethod.GET)
    public void textToken(@RequestHeader("Auth-Token") String token) {
        userService.testToken(token);
    }

    @RequestMapping(value ="/getName", method = RequestMethod.GET)
    public ResponseEntity getName(@RequestHeader("Auth-token") String token){
        String login = tokenHandler.extractUserLogin(token).orElse(null);
        Account account = userRepository.getAccountByLogin(login)
                .orElseThrow(() -> new ActivateAccountException("ERROR"));
        return ResponseEntity.ok(account.getName() + ' ' + account.getSurname());
    }
}
