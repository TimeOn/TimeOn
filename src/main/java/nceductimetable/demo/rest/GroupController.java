package nceductimetable.demo.rest;

import nceductimetable.demo.dto.GroupPeopleDTO;
import nceductimetable.demo.dto.GroupPeopleDTOGet;
import nceductimetable.demo.model.GroupPeople;
import nceductimetable.demo.services.GroupPeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GroupController {

    private final GroupPeopleService groupPeopleService;

    @Autowired
    public GroupController(GroupPeopleService groupPeopleService) {
        this.groupPeopleService = groupPeopleService;
    }

    @RequestMapping(value = "/group/create", method = RequestMethod.POST)
    public int createGroup(@RequestHeader("Auth-Token") String token, @RequestBody GroupPeopleDTO newGroupPeople) {
        return groupPeopleService.createGroupPeople(token, newGroupPeople);
    }

    @RequestMapping(value = "/group/{groupId}", method = RequestMethod.GET)
    public GroupPeopleDTO getGroupPeople(@PathVariable int groupId) {
        return groupPeopleService.getGroupPeopleById(groupId);
    }

    @RequestMapping(value = "/group/{groupId}", method = RequestMethod.PUT)
    public int updateGroup(@PathVariable int groupId, @RequestBody GroupPeopleDTO newGroupPeople, @RequestHeader("Auth-Token") String token) {
        return groupPeopleService.updateGroupPeople(groupId, newGroupPeople, token);
    }

    @RequestMapping(value = "/group/{groupId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteGroup(@PathVariable int groupId, @RequestHeader("Auth-Token") String token) {
        groupPeopleService.deleteGroup(groupId, token);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/group/add/{groupId}", method = RequestMethod.PUT)
    public int addAccountInGroup(@PathVariable(value = "groupId") int groupId, @RequestBody List<String> addedAccountLogins) {
        return groupPeopleService.addMemberInGroup(groupId, addedAccountLogins);
    }

    @RequestMapping(value = "/group/remove/{groupId}", method = RequestMethod.PUT)
    public int deleteAccountFromGroup(@PathVariable(value = "groupId") int groupId, @RequestBody List<String> deletedAccountLogins){
        return groupPeopleService.deleteMemberFromGroup(groupId, deletedAccountLogins);
    }

    @RequestMapping(value="/group/my", method=RequestMethod.GET)
    public List<GroupPeopleDTOGet> getMyGroups(@RequestHeader("Auth-Token") String token){
        return groupPeopleService.getMyGroups(token);
    }

    @RequestMapping(value="/group/user/{id}", method = RequestMethod.GET)
    public List<GroupPeopleDTOGet> getUserGroups(@PathVariable(value="id") int id){
        return groupPeopleService.getUserGroups(id);
    }

    @RequestMapping(value="/group/search", method = RequestMethod.GET)
    public List<GroupPeopleDTOGet> searchGroupByName(@RequestHeader("Auth-Token") String token,
                                                  @RequestParam("groupName") String groupName){
        return groupPeopleService.searchGroup(groupName);
    }
}
