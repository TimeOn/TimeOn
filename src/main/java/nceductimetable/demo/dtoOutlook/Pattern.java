package nceductimetable.demo.dtoOutlook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.mapping.Collection;

import java.util.List;

@Data
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pattern {
    private String type;
    private int interval;
    private int month;
    private int dayOfMonth;
    private String[] daysOfWeek;
    private String firstDayOfWeek;
    private String index;
}
