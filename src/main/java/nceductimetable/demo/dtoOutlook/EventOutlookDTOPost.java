package nceductimetable.demo.dtoOutlook;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;


@Data
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventOutlookDTOPost {
    private String subject;
    private Body body;
    private StartOrEnd start;
    private StartOrEnd end;
    private Recurrence recurrence;
    private Location location;
    private List<Attendee> attendees;
   // private boolean isAllDay;
}
