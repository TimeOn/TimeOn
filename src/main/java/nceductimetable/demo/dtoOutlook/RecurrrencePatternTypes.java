package nceductimetable.demo.dtoOutlook;

public enum RecurrrencePatternTypes {
    daily, weekly, absoluteMonthly, relativeMonthly, absoluteYearly, relativeYearly
}
