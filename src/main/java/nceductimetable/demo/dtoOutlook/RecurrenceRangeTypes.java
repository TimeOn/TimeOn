package nceductimetable.demo.dtoOutlook;

public enum RecurrenceRangeTypes {
    endDate, noEnd, numbered
}
