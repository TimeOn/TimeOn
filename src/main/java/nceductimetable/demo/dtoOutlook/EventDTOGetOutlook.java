package nceductimetable.demo.dtoOutlook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventDTOGetOutlook {
    private String id;
    private String subject;
    private Body body;
    private StartOrEnd start;
    private StartOrEnd end;
    private Recurrence recurrence;
    private Location location;
    private ResponseStatus responseStatus;
    //private LocalDateTime lastModifiedDateTime;
    //private List<Attendee> attendees;
    // private boolean isAllDay;
}

