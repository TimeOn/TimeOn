package nceductimetable.demo.dtoOutlook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nceductimetable.demo.dtoOutlook.EmailAddress;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipient {
    private EmailAddress emailAddress;

    public EmailAddress getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(EmailAddress emailAddress) {
        this.emailAddress = emailAddress;
    }
}