package nceductimetable.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class RegistrationDataException extends RuntimeException {

    public RegistrationDataException(String message) {
        super(message);
    }
}
