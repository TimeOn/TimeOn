package nceductimetable.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.LOCKED)
public class GoogleExeption  extends RuntimeException{
    public GoogleExeption(String message) {
        super(message);
    }
}
