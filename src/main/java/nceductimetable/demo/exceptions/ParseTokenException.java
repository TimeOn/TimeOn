package nceductimetable.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.LOCKED)
public class ParseTokenException extends RuntimeException {

    public ParseTokenException(String message) {
        super(message);
    }
}
