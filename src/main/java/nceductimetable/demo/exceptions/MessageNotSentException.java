package nceductimetable.demo.exceptions;

public class MessageNotSentException extends RuntimeException {

    public MessageNotSentException(String message, Throwable e) {
        super(message, e);
    }

    public MessageNotSentException(String message) {
        super(message);
    }
}
