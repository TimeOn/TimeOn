package nceductimetable.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class AcceptGroupException extends RuntimeException {

    public AcceptGroupException (String message) {
        super(message);
    }
}
