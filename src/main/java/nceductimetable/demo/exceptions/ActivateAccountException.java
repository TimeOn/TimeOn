package nceductimetable.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class ActivateAccountException extends RuntimeException {

    public ActivateAccountException(String message) {
        super(message);
    }
}
