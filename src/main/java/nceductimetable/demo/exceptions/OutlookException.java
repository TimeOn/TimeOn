package nceductimetable.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.LOCKED)
public class OutlookException extends RuntimeException{
    public OutlookException(String message) {
        super(message);
    }
}
