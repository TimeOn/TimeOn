package nceductimetable.demo.dtoGet;

import lombok.Data;

import java.util.List;

@Data
public class EventDTOWithAnswers {
    private int id;

   // private String ext_id;

    private String name;

    private String dateTimeBegin;

    private String dateTimeEnd;

    private List<EventAccountsDTOGet> accountsDTOGets;
}
