package nceductimetable.demo.dtoGet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import nceductimetable.demo.dtoOutlook.EmailAddress;

@Data
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class CalendarDTOGet {
    private String id;

    private String name;

    private String color;

    private EmailAddress owner;
}
