package nceductimetable.demo.dtoGet;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import nceductimetable.demo.enumForEvents.EventMembersStatus;

@Data
@EqualsAndHashCode
@ToString
public class EventDTOGetNotMy {
    private String dateTimeBegin;

    private String dateTimeEnd;

    //private EventMembersStatus status;
}
