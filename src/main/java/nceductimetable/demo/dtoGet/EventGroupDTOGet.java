package nceductimetable.demo.dtoGet;

import lombok.Data;

@Data
public class EventGroupDTOGet {
    private boolean isImportant;
    private int group;
    private String name;
}
