package nceductimetable.demo.dtoGet;

import lombok.Data;
import nceductimetable.demo.enumForEvents.EventMembersStatus;

@Data
public class EventAccountsDTOGet {
    private String login;
   // private String name;
    private String extSystem;
    private boolean isImportant;
    private EventMembersStatus status;
}
