package nceductimetable.demo.dtoGet;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
public class ExternalAccountDTOGet {
    private Integer id;
    private String userName;
    private String userEmail;

}
