package nceductimetable.demo.dtoGet;

import lombok.Data;
import nceductimetable.demo.dtoPost.EventAccountsDTOPost;
import nceductimetable.demo.dtoPost.EventGroupsDTOPost;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class EventDTOGet {
    private String name;

    private String address;

    private String dateTimeBegin;

    private String dateTimeEnd;

    //private String timeZone;

    private String description;

    private String recurrence;

    // private boolean isAllDay;

    private List<Integer> calendars;

    private List<EventAccountsDTOGet> membersAccounts;

    private List<EventGroupDTOGet> membersGroups;

}
