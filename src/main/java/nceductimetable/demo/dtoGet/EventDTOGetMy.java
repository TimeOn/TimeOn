package nceductimetable.demo.dtoGet;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import nceductimetable.demo.dtoPost.EventAccountsDTOPost;
import nceductimetable.demo.dtoPost.EventGroupsDTOPost;
import nceductimetable.demo.enumForEvents.EventMembersStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class EventDTOGetMy {
    private int id;

    private String ext_id;

    private String name;

    private String dateTimeBegin;

    private String dateTimeEnd;

    //private String lastModifiedDateTime;

    //private String seriesId;

    private EventMembersStatus status;

    private List<Integer> calendars;

    //private List<EventAccountsDTOPost> membersAccounts;

    //private List<EventGroupsDTOPost> membersGroups;

}
