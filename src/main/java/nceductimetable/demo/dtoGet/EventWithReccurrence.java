package nceductimetable.demo.dtoGet;

import lombok.Data;
import nceductimetable.demo.model.Event;

import java.sql.Timestamp;

@Data
public class EventWithReccurrence extends Event {
    private Timestamp start_time;
    private Timestamp end_time;
}
