package nceductimetable.demo.auth;

import lombok.NonNull;
import nceductimetable.demo.dto.AccountAuth;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UserAuthentication implements Authentication {

    private final AccountAuth account;
    private boolean authenticated = true;

    public UserAuthentication(@NonNull AccountAuth account) {
        this.account = account;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return account.getAuthorities();
    }

    @Override
    public Object getCredentials() {
        return account.getPassword();
    }

    @Override
    public Object getDetails() {
        return account;
    }

    @Override
    public Object getPrincipal() {
        return account;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        this.authenticated = isAuthenticated();
    }

    @Override
    public String getName() {
        return account.getUsername();
    }
}
