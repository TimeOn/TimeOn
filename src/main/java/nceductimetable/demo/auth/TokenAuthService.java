package nceductimetable.demo.auth;


import lombok.NonNull;
import nceductimetable.demo.services.TokenHandler;
import nceductimetable.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Component
public class TokenAuthService {
    private static final String AUTH_HEADER_NAME = "Auth-Token";
    private final TokenHandler tokenHandler;
    private final UserService userService;

    @Autowired
    public TokenAuthService(TokenHandler tokenHandler, UserService userService) {
        this.tokenHandler = tokenHandler;
        this.userService = userService;
    }

    public Optional<Authentication> getAuthentication(@NonNull HttpServletRequest request) {
        return Optional
                .ofNullable(request.getHeader(AUTH_HEADER_NAME))
                .flatMap(tokenHandler::extractUserLogin)
                .flatMap(userService::findByLogin)
                .map(UserAuthentication::new);
    }
}

