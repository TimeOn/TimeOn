package nceductimetable.demo.dtoPost;

import lombok.Data;

@Data
public class EventGroupsDTOPost {
    private boolean isImportant;
    private int group;
    //int id;
}
