package nceductimetable.demo.dtoPost;

import lombok.Data;

@Data
public class EventAccountsDTOPost {
    private String login;
    private String name;
    private String extSystem;
    private boolean isImportant;
}
