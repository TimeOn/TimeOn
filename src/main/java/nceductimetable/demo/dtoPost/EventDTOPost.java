package nceductimetable.demo.dtoPost;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

@Data
public class EventDTOPost {
    private String name;

    private String address;

    private LocalDateTime dateTimeBegin;

    private LocalDateTime dateTimeEnd;

    private String timeZone;

    private String description;

    private String recurrence;

   // private boolean isAllDay;

    private List<Integer> calendars;

    private List<EventAccountsDTOPost> membersAccounts;

    private List<EventGroupsDTOPost> membersGroups;

}
