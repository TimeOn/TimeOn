package nceductimetable.demo.services;

import lombok.extern.slf4j.Slf4j;

import nceductimetable.demo.exceptions.MessageNotSentException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

@Service("sendingLetter")
@Slf4j
public class Mail {

    @Value("${mail.pwd}")
    private String password;

    @Value("${mail.name}")
    private String name;

    @Value("${mail.smtps.user}")
    private String user;

    @ConfigurationProperties(prefix = "mail")
    private YamlPropertiesFactoryBean yamlProperties() {
        YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
        factory.setResources(new ClassPathResource("application.yml"));
        return factory;
    }

    public void sendLetter(String recipientAddress, String subject, String text) {
        Properties properties = yamlProperties().getObject();
        //properties.put("mail.smtp.starttls.enable","true");
        //properties.put("mail.smtp.debug", "true");
        //properties.put("mail.smtp.auth", "true");
        //properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        //properties.put("mail.smtp.socketFactory.fallback", "false");

        Session mailSession = Session.getDefaultInstance(properties);
        try {
            MimeMessage message = new MimeMessage(mailSession);
            message.setFrom(new InternetAddress(user, name));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientAddress));
            message.setSubject(subject);
            message.setText(text);

            Transport transport = mailSession.getTransport();
            log.info("Try to connect with mail server\n");
            transport.connect(user, password);
            log.info("Try to send message\n");
            transport.sendMessage(message, message.getAllRecipients());
            log.info("Message sent successfully\n");
            transport.close();
        } catch (MessageNotSentException | UnsupportedEncodingException | MessagingException e) {
            log.error("Message not sent", e);
            throw new MessageNotSentException("Message not sent");
        }
    }
}
