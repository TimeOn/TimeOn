package nceductimetable.demo.services;

import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.calendarSystem.*;
import nceductimetable.demo.calendarSystem.oauth.AuthHelper;
import nceductimetable.demo.calendarSystem.oauth.IdToken;
import nceductimetable.demo.calendarSystem.oauth.TokenResponse;
import nceductimetable.demo.dto.AccountForSearch;

import nceductimetable.demo.dto.AccountReturnedById;
import nceductimetable.demo.dtoGet.ExternalAccountDTOGet;
import nceductimetable.demo.enumForEvents.ExternalSystem;
import nceductimetable.demo.exceptions.GoogleExeption;
import nceductimetable.demo.exceptions.OutlookException;
import nceductimetable.demo.mappers.AccountMapper;
import nceductimetable.demo.mappers.ExternalAccountMapper;
import nceductimetable.demo.model.*;
import nceductimetable.demo.repositories.*;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import nceductimetable.demo.repositories.AccountRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static java.lang.Math.toIntExact;

@Service
@Slf4j
public class AccountService {
    private final AccountRepository accountRepository;
    private final ExternalAccountRepository externalAccountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository,
                          ExternalAccountRepository externalAccountRepository) {
        this.accountRepository = accountRepository;
        this.externalAccountRepository = externalAccountRepository;
    }


    public List<ExternalAccountDTOGet> getMyExternalAccounts(int accountId){
        List<ExternalAccount> externalAccounts=externalAccountRepository.findExternalAccountsByAccount(this.getAccountById(accountId));
        List<ExternalAccountDTOGet> externalAccountDTOGets=new ArrayList<>();
        ExternalAccountMapper externalAccountMapper=Mappers.getMapper(ExternalAccountMapper.class);
        for (ExternalAccount externalAccount:externalAccounts)
            externalAccountDTOGets.add(externalAccountMapper.externalAccountToExternalAccountDTOGet(externalAccount));
        return externalAccountDTOGets;
    }

    public Account getAccountById(int accountId) {
        return accountRepository.findOne(accountId);
    }

    public List<AccountForSearch> searchAccounts(String searchRequest, int count, int offSet) {
        Pageable pageable = new PageRequest(offSet, count);
        List<Account> accounts = accountRepository.findByLoginLikeIgnoreCase(searchRequest.concat("%"), pageable);
        //searchAccounts(searchRequest);//,count);//,offSet);
        //AccountMapper accountMapper = Mappers.getMapper(AccountMapper.class);
        List<AccountForSearch> accountEventDTOS = new ArrayList<>();
        for (Account account : accounts) {
            AccountForSearch accountForSearch =new AccountForSearch();
            accountForSearch.setLogin(account.getLogin());
            //accountForSearch.setEmail(account.getEmail());
            accountForSearch.setId(account.getId());
            accountForSearch.setName(account.getName());
            //accountForSearch.setSurname(account.getSurname());

            /*if (account.getExternalAccounts()!=null) {
                List<String> ext_accList = new ArrayList<>();
                for (ExternalAccount extAcc : account.getExternalAccounts()) {

                    ext_accList.add(extAcc.getUserEmail());

                }
                accountForSearch.setExternalAccounts(ext_accList);
            }
*/
            accountEventDTOS.add(accountForSearch);

        }
        return accountEventDTOS;

    }

    public AccountReturnedById getUserById(int myId, int userId){
        Account account=accountRepository.findOne(userId);
        if (account!=null)
        {
            AccountReturnedById res=new AccountReturnedById();
            res.setLogin(account.getLogin());
            res.setName(account.getName());
            res.setId(userId);
            res.setVisible(account.isVisible());
            //if (account.isVisible()) {
            res.setEmail(account.getEmail());
            res.setSurname(account.getSurname());
            List<String> externals=new ArrayList<>();
            if (account.getExternalAccounts()!=null) {
                for (ExternalAccount ext : account.getExternalAccounts())
                    externals.add(ext.getUserEmail());
                res.setExternalAccounts(externals);
            }
            //}
            return res;
        }
        return null;
    }

    public Account findByLogin(String login){
        return accountRepository.findAccountByLogin(login);
    }

    public Account searchByExternalLogin(String login) {
        ExternalAccount externalAccount = externalAccountRepository.findExternalAccountByUserEmail(login);
        if (externalAccount != null)
            return externalAccount.getAccount();
        return accountRepository.findAccountByLogin(login);
    }

    public ExternalAccount findExtAccountByName(String name){
        return externalAccountRepository.findExternalAccountByUserName(name);
    }

    public ExternalAccount findExtAccountByEmail(String email){
        return externalAccountRepository.findExternalAccountByUserEmail(email);
    }


    public ResponseEntity registrateExternalAccount(int accountId, String code, String idToken, String exp_nonce) {
        IdToken idTokenObj = IdToken.parseEncodedToken(idToken, exp_nonce.toString());
        if (idTokenObj != null) {
            TokenResponse tokenResponse = AuthHelper.getTokenFromAuthCode(code, idTokenObj.getTenantId());

            ExternalAccount externalAccount = new ExternalAccount();
            externalAccount.setAccount(accountRepository.findOne(accountId));
            externalAccount.setTenantId(idTokenObj.getTenantId());
            externalAccount.setUserName(idTokenObj.getName());//name or email or???

            externalAccount.setToken(tokenResponse);
            /*externalAccount.setAccessToken(tokenResponse.getAccessToken());
            externalAccount.setExpirationTime(tokenResponse.getExpirationTime());
            externalAccount.setExpiresIn(tokenResponse.getExpiresIn());*/
            externalAccount.setExternalSystem(ExternalSystem.OUTLOOK.name());
            /*externalAccount.setIdToken(tokenResponse.getIdToken());
            externalAccount.setRefreshToken(tokenResponse.getRefreshToken());
            externalAccount.setScope(tokenResponse.getScope());
            externalAccount.setTokenType(tokenResponse.getTokenType());*/

            OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokenResponse.getAccessToken(),idTokenObj.getEmail(), Optional.empty());
            Map<String, Object> user;
            try {
                user = outlookService.getCurrentUser().execute().body();
                log.info("{} user", user);
                externalAccount.setUserEmail(user.get("userPrincipalName").toString());
                externalAccount.setExternalId(user.get("id").toString());

            } catch (IOException e) {
                throw new OutlookException(e.getMessage());
            }
            ExternalAccount old=externalAccountRepository.findExternalAccountByUserEmail(externalAccount.getUserEmail());
            if (old!=null)
                externalAccount.setId(old.getId());
            externalAccountRepository.save(externalAccount);
            // this.getOutlookCalendars(externalAccount);

            return ResponseEntity.ok().build();

        }
        return ResponseEntity.notFound().build();
    }

    /* public void getOutlookCalendars(ExternalAccount externalAccount) {
         TokenResponse tokens = externalAccount.getAccessToken();
         if (tokens == null) {
             throw new OutlookException("not signed in user");
         }

         String tenantId = externalAccount.getTenantId();

         tokens = AuthHelper.ensureTokens(tokens, tenantId);

         String email = externalAccount.getUserEmail();

         OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email);

         String properties = "id, name, color";

         try {
             List<Map<String, Object>> calendars = outlookService.getCalendars(properties).execute().body();
             for (Map<String, Object> map : calendars) {
                 Calendar calendar = new Calendar();
                 calendar.setColor(map.get("color").toString());
                 calendar.setExternalAccount(externalAccount);
                 calendar.setAccount(externalAccount.getAccount());
                 calendar.setExternalId(map.get("id").toString());
                 calendar.setName(map.get("name").toString());
             }
         } catch (IOException e) {
             throw new OutlookException(e.getMessage());
         }


     }*/
    private  com.google.api.services.calendar.Calendar client;

    public ResponseEntity googleRegistrateExternalAccount(int accountId, String code) {
        if (code != null) {
            try {
                com.google.api.client.auth.oauth2.TokenResponse tokenResponse = GoogleCalendar.getTokenFromLoginCode(code);
                client = GoogleCalendar.getServiceFromToken(tokenResponse);
                com.google.api.services.calendar.model.Calendar calendar = client.calendars().get("primary").execute();

                ExternalAccount externalAccount = new ExternalAccount();

                externalAccount.setAccount(accountRepository.findOne(accountId));
                externalAccount.setTenantId(null);
                externalAccount.setUserName(calendar.getId());//name or email or???

                externalAccount.setAccessToken(tokenResponse.getAccessToken());
                externalAccount.setExpirationTime(null);
                externalAccount.setExpiresIn(toIntExact(tokenResponse.getExpiresInSeconds()));

                externalAccount.setExternalSystem(ExternalSystem.GOOGLE.name());
                externalAccount.setIdToken(null);
                externalAccount.setRefreshToken(tokenResponse.getRefreshToken());
                externalAccount.setScope(tokenResponse.getScope());
                externalAccount.setTokenType(tokenResponse.getTokenType());

                externalAccount.setUserEmail(calendar.getId());
                externalAccount.setExternalId(calendar.getId());
                ExternalAccount old = externalAccountRepository.findExternalAccountByUserEmail(externalAccount.getUserEmail());
                if (old != null)
                    externalAccount.setId(old.getId());
                externalAccountRepository.save(externalAccount);
                // this.getOutlookCalendars(externalAccount);

                return ResponseEntity.ok().build();
            } catch (Exception e) {
                log.error("something went wrong with external account creation");
                throw new GoogleExeption("something wrong with google external account creation");
            }

        }
        return ResponseEntity.notFound().build();

    }


}



