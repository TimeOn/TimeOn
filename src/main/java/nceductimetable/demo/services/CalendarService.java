package nceductimetable.demo.services;

import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.dtoGet.ExternalAccountDTOGet;
import nceductimetable.demo.dtoOutlook.EmailAddress;
import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.model.Calendar;
import nceductimetable.demo.model.ExternalAccount;
import nceductimetable.demo.repositories.CalendarRepository;
import nceductimetable.demo.repositories.ExternalAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class CalendarService {
    private final CalendarRepository calendarRepository;
    //private final AccountRepository accountRepository;
    private final AccountService accountService;
    private final ExternalAccountRepository externalAccountRepository;


    @Autowired
    public CalendarService(CalendarRepository calendarRepository,
                           //AccountRepository accountRepository,
                           AccountService accountService,
                           ExternalAccountRepository externalAccountRepository){
        this.calendarRepository=calendarRepository;
        //this.accountRepository=accountRepository;
        this.accountService=accountService;
        this.externalAccountRepository=externalAccountRepository;
    }

    public void createCalendar(Account account, String name, String color){
        Calendar calendar=new Calendar();
        calendar.setColor(color);
        calendar.setAccount(account);
        calendar.setName(name);
        calendarRepository.save(calendar);
    }

    public void createCalendars(int accountId, List<CalendarDTOGet> calendars){
        for (CalendarDTOGet calendarDTOGet:calendars){
            Calendar calendar=new Calendar();
            calendar.setName(calendarDTOGet.getName());
            calendar.setExternalId(calendarDTOGet.getId());
            calendar.setAccount(accountService.getAccountById(accountId));
            if (calendarDTOGet.getOwner()!=null) {
                ExternalAccount externalAccount = accountService.findExtAccountByEmail(calendarDTOGet.getOwner().getAddress());
                if (externalAccount!=null)
                    calendar.setExternalAccount(externalAccount);
            }
            calendar.setColor(calendarDTOGet.getColor());
            calendarRepository.save(calendar);
        }

    }

    public List<CalendarDTOGet> getMyCalendars(int accountId){
        List<Calendar> calendars=calendarRepository.findCalendarByAccount(accountService.getAccountById(accountId));
        List<CalendarDTOGet> calendarDTOGets=new ArrayList<>();
        for (Calendar calendar:calendars){
            CalendarDTOGet calendarDTOGet=new CalendarDTOGet();
            calendarDTOGet.setColor(calendar.getColor());
            calendarDTOGet.setId(calendar.getId().toString());
            calendarDTOGet.setName(calendar.getName());
            if (calendar.getExternalAccount()!=null) {
                EmailAddress owner=new EmailAddress();
                owner.setAddress(calendar.getExternalAccount().getUserEmail());
                owner.setName(calendar.getExternalAccount().getUserName());
                calendarDTOGet.setOwner(owner);
            }
            calendarDTOGets.add(calendarDTOGet);
        }
        return calendarDTOGets;
    }

    //public

    //public void addExternalCalendars

}
