package nceductimetable.demo.services;

import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.dto.*;
import nceductimetable.demo.dtoGet.EventDTOGetNotMy;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.jws.soap.SOAPBinding;
import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Slf4j
public class TimeService {

    private EventService eventService;
    private UserRepository userRepository;

    @Autowired
    public TimeService(EventService eventService, UserRepository userRepository) {
        this.eventService = eventService;
        this.userRepository = userRepository;
    }

    public TreeMap calendar(LocalDateTime begin, LocalDateTime end, List<String> accounts) {
        TreeMap<LocalDateTime, Integer> mas = new TreeMap<>();
        begin = begin.withSecond(0).withNano(0);
        end = end.withSecond(0).withNano(0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime dateTime = begin;
        while (dateTime.isBefore(end)) {
            mas.put(dateTime, 0);
            dateTime = dateTime.plusMinutes(5);
        }
        for (String account : accounts) {
            //Account tempaccount = userRepository.getAccountById(account)
            //  .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
            List<EventDTOGetNotMy> eventInfoDTO = eventService.getOthersEvents(account, begin, end, "Europe/Moscow", 123);
            for (EventDTOGetNotMy eventInfoDTO1 : eventInfoDTO) {
                LocalDateTime dateBegin = LocalDateTime.parse(eventInfoDTO1.getDateTimeBegin(), formatter);
                LocalDateTime dateEnd = LocalDateTime.parse(eventInfoDTO1.getDateTimeEnd(), formatter);
                if (dateBegin.withSecond(0).withNano(0).isBefore(begin) && (dateEnd.withSecond(0).withNano(0).isAfter(begin))) {
                    if (dateEnd.withSecond(0).withNano(0).isAfter(end.withSecond(0).withNano(0))) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(dateEnd.withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else if (dateBegin.withSecond(0).withNano(0).isBefore(end.withSecond(0).withNano(0)) && dateEnd.withSecond(0).withNano(0).isAfter(end.minusMinutes(1).withSecond(0).withNano(0))) {
                    if (dateBegin.withSecond(0).withNano(0).isBefore(begin.withSecond(0).withNano(0))) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(dateBegin.withSecond(0).withNano(0)))
                            temp = temp.plusMinutes(5);
                        while (temp.isBefore(end.withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else {
                    LocalDateTime temp = begin.withSecond(0).withNano(0);
                    while (temp.isBefore(dateBegin.withSecond(0).withNano(0)))
                        temp = temp.plusMinutes(5);
                    while (temp.isBefore(dateEnd.plusMinutes(1).withSecond(0).withNano(0))) {
                        mas.replace(temp, mas.get(temp) + 10);
                        temp = temp.plusMinutes(5);
                    }
                }
            }
        }
        List<LocalDateTime> delkeys = new ArrayList<>();
        for (Map.Entry entry : mas.entrySet()) {
            LocalDateTime temp = (LocalDateTime) entry.getKey();
            if (temp.getHour() < 8)
                delkeys.add((LocalDateTime) entry.getKey());
        }
        for (LocalDateTime key : delkeys) {
            mas.remove(key);
        }
        delkeys.clear();
        int delvalue = 1;
        for (Map.Entry entry : mas.entrySet()) {
            if ((int) entry.getValue() == delvalue) {
                delkeys.add((LocalDateTime) entry.getKey());
            }
            delvalue = (int) entry.getValue();
        }
        for (LocalDateTime key : delkeys) {
            mas.remove(key);
        }
        mas.put(end.minusMinutes(5), 0);
        return mas;
    }

    /*public List<PickingTime> calendarTime(LocalDateTime begin, LocalDateTime end, List<String> accounts) {
        TreeMap<LocalDateTime, Integer> mas = new TreeMap<>();
        begin = begin.withSecond(0).withNano(0);
        end = end.withSecond(0).withNano(0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = begin;
        while (dateTime.isBefore(end)) {
            mas.put(dateTime, 0);
            dateTime = dateTime.plusMinutes(5);
        }
        for (String account : accounts) {
            //Account tempaccount = userRepository.getAccountById(account)
            //  .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
            List<EventDTOGetNotMy> eventInfoDTO = eventService.getOthersEvents(account, begin, end, "Europe/Moscow", 2);
            for (EventDTOGetNotMy eventInfoDTO1 : eventInfoDTO) {
                LocalDateTime dateBegin = LocalDateTime.parse(eventInfoDTO1.getDateTimeBegin(), formatter);
                LocalDateTime dateEnd = LocalDateTime.parse(eventInfoDTO1.getDateTimeEnd(), formatter);
                if (dateBegin.withSecond(0).withNano(0).isBefore(begin) && (dateEnd.withSecond(0).withNano(0).isAfter(begin))) {
                    if (dateEnd.withSecond(0).withNano(0).isAfter(end.withSecond(0).withNano(0))) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(dateEnd.withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else if (dateBegin.withSecond(0).withNano(0).isBefore(end.withSecond(0).withNano(0)) && dateEnd.withSecond(0).withNano(0).isAfter(end.minusMinutes(1).withSecond(0).withNano(0))) {
                    if (dateBegin.withSecond(0).withNano(0).isBefore(begin.withSecond(0).withNano(0))) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(dateBegin.withSecond(0).withNano(0)))
                            temp = temp.plusMinutes(5);
                        while (temp.isBefore(end.withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else {
                    LocalDateTime temp = begin.withSecond(0).withNano(0);
                    while (temp.isBefore(dateBegin.withSecond(0).withNano(0)))
                        temp = temp.plusMinutes(5);
                    while (temp.isBefore(dateEnd.plusMinutes(1).withSecond(0).withNano(0))) {
                        mas.replace(temp, mas.get(temp) + 10);
                        temp = temp.plusMinutes(5);
                    }
                }
            }
        }
        List<LocalDateTime> delkeys = new ArrayList<>();
        int delvalue = 1;
        for (Map.Entry entry : mas.entrySet()) {
            if ((int) entry.getValue() == delvalue) {
                delkeys.add((LocalDateTime) entry.getKey());
            }
            delvalue = (int) entry.getValue();
        }
        for (LocalDateTime key : delkeys) {
            mas.remove(key);
        }
        for (Map.Entry entry : mas.entrySet()) {
            List<AccountEmployment> employments = new ArrayList<>();
            PickingTime pickingTime = new PickingTime();
            for (String account : accounts) {
                Account tempaccount = userRepository.getAccountById(account)
                        .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
                if (eventService.getAccountEvents(tempaccount, (LocalDateTime) entry.getKey())) {
                    AccountEmployment accountEmployment = new AccountEmployment();
                    accountEmployment.setFirstName(tempaccount.getName());
                    accountEmployment.setLastName(tempaccount.getSurname());
                    employments.add(accountEmployment);
                }
            }
            pickingTime.setAccounts(employments);
            pickingTime.setTime((LocalDateTime) entry.getKey());
            pickingTime.setValue((Integer) entry.getValue());
            pickingTimes.add(pickingTime);
        }
        return pickingTimes;
    }*/
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public List<PickTimeInfo> pickTime(Integer time, LocalDateTime begin, LocalDateTime end, List<String> accounts) {
        int kol = 0;
        TreeMap<LocalDateTime, Integer> mas = new TreeMap<>();
        begin = begin.withSecond(0).withNano(0);
        end = end.withSecond(0).withNano(0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime dateTime = begin;
        while (dateTime.isBefore(end)) {
            mas.put(dateTime, 0);
            dateTime = dateTime.plusMinutes(5);
            kol++;
        }
        for (String account : accounts) {
            //Account tempaccount = userRepository.getAccountById(account)
            //  .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
            List<EventDTOGetNotMy> eventInfoDTO = eventService.getOthersEvents(account, begin, end, "Europe/Moscow", 123);
            for (EventDTOGetNotMy eventInfoDTO1 : eventInfoDTO) {
                LocalDateTime dateBegin = LocalDateTime.parse(eventInfoDTO1.getDateTimeBegin(), formatter);
                LocalDateTime dateEnd = LocalDateTime.parse(eventInfoDTO1.getDateTimeEnd(), formatter);
                if (dateBegin.withSecond(0).withNano(0).isBefore(begin) && (dateEnd.withSecond(0).withNano(0).isAfter(begin))) {
                    if (dateEnd.withSecond(0).withNano(0).isAfter(end.withSecond(0).withNano(0))) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(dateEnd.withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else if (dateBegin.withSecond(0).withNano(0).isBefore(end.withSecond(0).withNano(0)) && dateEnd.withSecond(0).withNano(0).isAfter(end.minusMinutes(1).withSecond(0).withNano(0))) {
                    if (dateBegin.withSecond(0).withNano(0).isBefore(begin.withSecond(0).withNano(0))) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(dateBegin.withSecond(0).withNano(0)))
                            temp = temp.plusMinutes(5);
                        while (temp.isBefore(end.withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else {
                    LocalDateTime temp = begin.withSecond(0).withNano(0);
                    while (temp.isBefore(dateBegin.withSecond(0).withNano(0)))
                        temp = temp.plusMinutes(5);
                    while (temp.isBefore(dateEnd.plusMinutes(1).withSecond(0).withNano(0))) {
                        mas.replace(temp, mas.get(temp) + 10);
                        temp = temp.plusMinutes(5);
                    }
                }
            }
        }
        List<LocalDateTime> delkeys = new ArrayList<>();
        for (Map.Entry entry : mas.entrySet()) {
            LocalDateTime temp = (LocalDateTime) entry.getKey();
            if (temp.getHour() < 8) {
                delkeys.add((LocalDateTime) entry.getKey());
                kol--;
            }
        }
        for (LocalDateTime key : delkeys) {
            mas.remove(key);
        }
        List<PickTimeInfo> times = new ArrayList<>();
        LocalDateTime timeBegin = LocalDateTime.now();
        LocalDateTime timeEnd = LocalDateTime.now();
        int j = 0;
        int value = 0;
        //for (int i = 0; i < accounts.size(); i++) {
        for (Map.Entry entry : mas.entrySet()) {
            if (kol < time / 5) {
                break;
            }
            LocalDateTime tempTime = (LocalDateTime) entry.getKey();
            timeBegin = (LocalDateTime) entry.getKey();
            timeEnd = timeBegin.plusMinutes(time);
            tempTime = tempTime.minusMinutes(5);

            for (int z = 0; z < time; z += 5) {
                tempTime = tempTime.plusMinutes(5);
                if (tempTime.getHour() == 0)
                    tempTime = tempTime.plusHours(8);
                //log.info("add {} in time {}", mas.get(tempTime), tempTime);
                value += mas.get(tempTime);
            }

            int masval[] = {9999, 9999, 9999};
            if (value < masval[0]) {
                masval[2] = masval[1];
                masval[1] = masval[0];
                masval[0] = value;
            } else if (value < masval[1]) {
                masval[2] = masval[1];
                masval[1] = value;
            } else if (value < masval[2]) {
                masval[2] = value;
            }
            PickTimeInfo newTimes = new PickTimeInfo();
            newTimes.setBegin(timeBegin);
            newTimes.setEnd(timeEnd);
            newTimes.setValue(value);
            List<AccountEmployment> employments = new ArrayList<>();
            for (String account : accounts) {
                Account tempaccount = userRepository.getAccountByLogin(account)
                        .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
                //List<EventInfoDTO> eventInfoDTO = eventService.getAccountEventsOnPeriodOfTime2(tempaccount, timeBegin, timeEnd);
                //if (eventService.getAccountEventsOnTime(tempaccount, timeBegin, timeEnd)) {
                /*for (tempTime = timeBegin; tempTime.isBefore(timeEnd); tempTime = tempTime.plusMinutes(5)) {
                    log.info("lolkek");
                    List<EventDTOGetNotMy> eventInfoDTO = eventService.getOthersEvents(account, begin, end, "Europe/Moscow", 2);
                    //if (eventService.getAccountEvents(tempaccount, tempTime)) {
                    if (!eventInfoDTO.isEmpty()) {
                        //log.info("{}", tempTime);
                        AccountEmployment accountEmployment = new AccountEmployment();
                        accountEmployment.setFirstName(tempaccount.getName());
                        accountEmployment.setLastName(tempaccount.getSurname());
                        accountEmployment.setTime(tempTime);
                        employments.add(accountEmployment);
                    }
                }*/
                AccountIsBusy eventInfoDTO = eventService.getAccountBusy(account, timeBegin, timeEnd, "Europe/Moscow", 123);
                if (eventInfoDTO != null) {
                    AccountEmployment accountEmployment = new AccountEmployment();
                    accountEmployment.setFirstName(tempaccount.getName());
                    accountEmployment.setLastName(tempaccount.getSurname());
                    employments.add(accountEmployment);
                }
            }
            newTimes.setAccounts(employments);
            times.add(newTimes);
            value = 0;
            kol--;
                /*if ((Integer) entry.getValue() == i * 10) {
                    if (j == 0) {
                        timeBegin = (LocalDateTime) entry.getKey();
                    }
                    timeEnd = (LocalDateTime) entry.getKey();
                    j++;
                    value = value + (i * 10);
                } else {
                    j = 0;
                    value = 0;
                }
                if (j == time / 5) {
                    PickTimeInfo newTimes = new PickTimeInfo();
                    newTimes.setBegin(timeBegin);
                    newTimes.setEnd(timeEnd);
                    newTimes.setValue(value);
                    List<AccountEmployment> employments = new ArrayList<>();
                    for (Integer account : accounts) {
                        Account tempaccount = userRepository.getAccountById(account)
                                .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
                        if (eventService.getAccountEventsOnTime(tempaccount, timeBegin, timeEnd)) {
                            AccountEmployment accountEmployment = new AccountEmployment();
                            accountEmployment.setFirstName(tempaccount.getName());
                            accountEmployment.setLastName(tempaccount.getSurname());
                            employments.add(accountEmployment);
                        }
                    }
                    newTimes.setAccounts(employments);
                    times.add(newTimes);
                    value = 0;
                    j = 0;
                }*/
        }
        //}
        /*List<PickTimeInfo> newtimes = new ArrayList<>();
        for(PickTimeInfo pickTimeInfo : times){
            newtimes.add(pickTimeInfo);
            if(newtimes.size() == 3){
                break;
            }
        }*/
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        List<PickTimeInfo> newtimes = new ArrayList<>();
        times.sort(new Comparator<PickTimeInfo>() {
            @Override
            public int compare(PickTimeInfo o1, PickTimeInfo o2) {
                if (o1.getValue() > o2.getValue()) {
                    return 1;
                } else if (o1.getValue() < o2.getValue()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        int temp = 0;
        for (PickTimeInfo pickTimeInfo : times) {
            if (temp == 3) {
                break;
            }
            newtimes.add(pickTimeInfo);
            temp++;
        }
        return newtimes;
    }


    /*public List<AccountEmployment> employment(List<Integer> accounts, LocalDateTime time) {
        List<AccountEmployment> employments = new ArrayList<>();
        for (Integer account : accounts) {
            Account tempaccount = userRepository.getAccountById(account)
                    .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
            if (eventService.getAccountEvents(tempaccount, time)) {
                AccountEmployment accountEmployment = new AccountEmployment();
                accountEmployment.setFirstName(tempaccount.getName());
                accountEmployment.setLastName(tempaccount.getSurname());
                employments.add(accountEmployment);
            }
        }
        return employments;
    }*/
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public List<AccountEmploymentTime> employmentnew(List<String> accounts, LocalDateTime begin, LocalDateTime end) {
        List<AccountEmploymentTime> employments = new ArrayList<>();
        for (String account: accounts){
            Account tempaccount = userRepository.getAccountByLogin(account)
                    .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
            AccountIsBusy eventInfoDTO = eventService.getAccountBusy(account, begin, end, "Europe/Moscow", 123);
            if (eventInfoDTO != null) {
                AccountEmploymentTime accountEmployment = new AccountEmploymentTime();
                accountEmployment.setFirstName(tempaccount.getName());
                accountEmployment.setLastName(tempaccount.getSurname());
                accountEmployment.setEmp(true);
                employments.add(accountEmployment);
            }
        }
        return employments;
    }
}
    /*
    public ResponseEntity<TreeMap> meetingTime(TimePickingIn timePicking) {
        TimePicking picking = new TimePicking();
        picking.setBegin(timePicking.getBegin());
        picking.setEnd(timePicking.getEnd());
        List<UserTime> userTime = new ArrayList<>();
        for (Integer account : timePicking.getAccounts()) {
            Account tempaccount = userRepository.getAccountById(account)
                    .orElseThrow(() -> new UsernameNotFoundException("user " + account + " was not found!"));
            List<EventInfoDTO> eventInfoDTO = eventService.getAccountEventsOnPeriodOfTime2(tempaccount, timePicking.getBegin(), timePicking.getEnd());
            UserTime infoUser = new UserTime();
            infoUser.setId(account);
            infoUser.setEvents(eventInfoDTO);
            userTime.add(infoUser);
        }
        picking.setEvents(userTime);
        HttpEntity<TimePicking> request = new HttpEntity<>(picking);
        RestTemplate restTemplate = new RestTemplate();
        //ResponseEntity<TreeMap> response = restTemplate.exchange("http://167.99.239.130:8080/meetingForTimeOn", HttpMethod.POST, request, TreeMap.class);
        return restTemplate.exchange("http://167.99.239.130:8080/meetingForTimeOn", HttpMethod.POST, request, TreeMap.class);
    }*/
//}
