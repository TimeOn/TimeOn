package nceductimetable.demo.services;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.exceptions.ParseTokenException;
import nceductimetable.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@Service
@Slf4j
public class TokenHandler {

    @Autowired
    private UserRepository userRepository;

    /*@Autowired
    public TokenHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }*/

    private SecretKey secretKey;

    @Autowired
    public TokenHandler(@Value("${security.jwt}") String jwtKey) {
        byte[] decodedKey = BaseEncoding.base64().decode(jwtKey);
        secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }

    public Optional<String> extractUserLogin(@NonNull String token) {
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims body = claimsJws.getBody();
            Optional<String> id = Optional
                    .ofNullable(body.getId())
                    .map(String::new);
            String tok = id.orElseThrow(() -> new UsernameNotFoundException("user was not found!"));
            int[] pos = new int[2];
            pos[0] = tok.indexOf("*");
            pos[1] = tok.indexOf("*", pos[0] + 1);
            String login = tok.substring(0, pos[0]);
            if (!userRepository.getAccountByLogin(login).isPresent()) {
                log.error("{} login not found", login);
                throw new ParseTokenException("user was not found");
            }
            String email = tok.substring(pos[0] + 1, pos[1]);
            if (!userRepository.getAccountByMail(email).isPresent()) {
                log.error("{} email not found", email);
                throw new ParseTokenException("email was not found");
            }
            return Optional.of(login);
        } catch (ParseTokenException e) {
            log.error("RarseTokenException", e);
            throw new ParseTokenException("RarseTokenException");
        }

    }

    public String generateAccessToken(@NonNull String email, @NonNull String login, @NonNull String password, @NonNull LocalDateTime expires) {
        String token = login + "*" + email + "*" + Hashing.sha512().hashString(password, StandardCharsets.UTF_8).toString();
        return Jwts.builder()
                .setId(token)
                .setExpiration(Date.from(expires.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }
}