package nceductimetable.demo.services;

import nceductimetable.demo.dto.AccountDTO;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AccountSettingsService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountSettingsService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void updateAccountData(int accountId, AccountDTO newAccountData) {
        Account account = accountRepository.findOne(accountId);
        account.setName(newAccountData.getName());
        account.setSurname(newAccountData.getSurname());
        account.setVisible(newAccountData.isVisible());
        //updating password (encrypt?)
        if (Objects.equals(account.getPassword(), newAccountData.getPassword()))
            account.setPassword(newAccountData.getNewPassword());
        accountRepository.save(account);
    }
}
