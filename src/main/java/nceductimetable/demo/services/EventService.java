package nceductimetable.demo.services;

import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;

import nceductimetable.demo.calendarSystem.MyEventJson;
import nceductimetable.demo.dto.AccountIsBusy;
import nceductimetable.demo.dtoGet.*;
import nceductimetable.demo.dtoOutlook.Attendee;
import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.dtoPost.EventAccountsDTOPost;
import nceductimetable.demo.dtoPost.EventGroupsDTOPost;
import nceductimetable.demo.enumForEvents.EventMembersStatus;
import nceductimetable.demo.enumForEvents.ExternalSystem;
import nceductimetable.demo.exceptions.MessageNotSentException;
import nceductimetable.demo.exceptions.OutlookException;
import nceductimetable.demo.mappers.EventDTOPostMapper;
import nceductimetable.demo.model.*;
import nceductimetable.demo.model.Calendar;
import nceductimetable.demo.repositories.*;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static java.lang.Math.abs;

@Service
@Slf4j
public class EventService {
    private final EventRepository eventRepository;
    private final EventAccountsRepository eventAccountsRepository;
    private final CalendarRepository calendarRepository;
    private final EventGroupsRepository eventGroupsRepository;
    private final EventDTOPostMapper eventDTOPostMapper = Mappers.getMapper(EventDTOPostMapper.class);
    private final AccountService accountService;
    private final GroupPeopleRepository groupPeopleRepository;
    private final TokenHandler tokenHandler;
    private final CalendarEventRepository calendarEventRepository;
    private final CalendarIntService calendarIntService;
    private final ExternalAccountRepository externalAccountRepository;
    private final CalendarService calendarService;
    private final EventExternalAccountsRepository externalAccountsRepository;
    private final Mail mail;
    @Autowired
    JdbcTemplate jdbcTemplate;


    @Autowired
    public EventService(EventRepository eventBaseRepository,
                        AccountService accountService,
                        CalendarRepository calendarRepository,
                        EventAccountsRepository eventAccountsRepository,
                        EventGroupsRepository eventGroupsRepository,
                        GroupPeopleRepository groupPeopleRepository,
                        TokenHandler tokenHandler,
                        CalendarEventRepository calendarEventRepository,
                        CalendarIntService calendarIntService,
                        ExternalAccountRepository externalAccountRepository,
                        CalendarService calendarService,
                        Mail mail,
                        EventExternalAccountsRepository externalAccountsRepository) {
        this.calendarRepository = calendarRepository;
        this.eventRepository = eventBaseRepository;
        this.eventAccountsRepository = eventAccountsRepository;
        this.eventGroupsRepository = eventGroupsRepository;
        this.groupPeopleRepository = groupPeopleRepository;
        this.accountService = accountService;
        this.tokenHandler = tokenHandler;
        this.calendarEventRepository = calendarEventRepository;
        this.calendarIntService = calendarIntService;
        this.externalAccountRepository = externalAccountRepository;
        this.calendarService = calendarService;
        this.mail = mail;
        this.externalAccountsRepository=externalAccountsRepository;
    }

    public AccountIsBusy getAccountBusy(String login, LocalDateTime dateTimeBegin, LocalDateTime dateTimeEnd, String timeZone, int me){
        List<EventDTOGetNotMy> events=this.getOthersEvents(login, dateTimeBegin, dateTimeEnd, timeZone, me);
        if (events==null)
                return null;
        if (events.size()==0)
            return null;
        AccountIsBusy result=new AccountIsBusy();
        if (accountService.findByLogin(login)!=null){
            result.setName(accountService.findByLogin(login).getName());
            result.setSurname(accountService.findByLogin(login).getSurname());
            return result;
        }
        if (accountService.findExtAccountByEmail(login)!=null)
            if (accountService.findExtAccountByEmail(login).getAccount().isVisible())
            {
                result.setName(accountService.findExtAccountByEmail(login).getAccount().getName());
                result.setSurname(accountService.findExtAccountByEmail(login).getAccount().getSurname());
                return result;
            }
        result.setName(login);
        result.setSurname(login);
        return result;


    }

    public List<EventDTOWithAnswers> getEventsWithAnswers(int myId, String timeZone){
        List<EventDTOWithAnswers> events = new ArrayList<>();
        List<Map<String, Object>> eventList = jdbcTemplate.query(
                "SELECT events.id, events.name, events.start_time," +
                        " events.start_time + make_interval(0,0,0,0,0,0,cast(events.duration / 1000 as int)) as end_date " +
                        " FROM " +
                        "  (SELECT e.*, CASE WHEN e.recurrence is not null " +
                        "       THEN unnest(get_occurrences(cast(e.recurrence as rrule), cast(e.date_time_begin as timestamp))) " +
                        "       ELSE e.date_time_begin END as start_time " +
                        "   FROM event e WHERE e.is_cancelled=false ) events " +
                        " JOIN event_accounts ea on " +
                        " ea.account_id=? and events.id=ea.event_id and ea.status = ? " +
                        " ORDER BY events.creation_date_time desc " +
                        ";"
                , new Object[]{myId, "creator"},
                (rs, rowNum) -> ImmutableMap.of(
                        "id", rs.getInt(1),
                        "name", rs.getString(2),
                        "start_time", rs.getString(3),
                        "end_date", rs.getString(4)
                ));
        for (Map<String, Object> obj : eventList) {

            EventDTOWithAnswers event = new EventDTOWithAnswers();
            String begin = obj.get("start_time").toString(),
                    end = obj.get("end_date").toString();
            String tmp[] = begin.split(" ");
            begin = tmp[0].concat("T").concat(tmp[1]);
            tmp = end.split(" ");
            end = tmp[0].concat("T").concat(tmp[1]);
            begin = ZonedDateTime.of(LocalDateTime.parse(begin), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();
            end = ZonedDateTime.of(LocalDateTime.parse(end), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();

            event.setDateTimeBegin(begin.substring(0, 16));
            event.setDateTimeEnd(end.substring(0, 16));
            event.setName(obj.get("name").toString());
            event.setId((int) obj.get("id"));
            event.setAccountsDTOGets(new ArrayList<>());
            List<EventAccounts> eventAccounts=eventRepository.findOne(event.getId()).getMembersAccounts();
            if (eventAccounts!=null)
                for (EventAccounts eventAccount:eventAccounts){
                if (!eventAccount.getStatus().equals(EventMembersStatus.creator.toString())) {
                    EventAccountsDTOGet member = new EventAccountsDTOGet();
                    member.setExtSystem(null);
                    member.setStatus(EventMembersStatus.valueOf(eventAccount.getStatus()));
                    member.setLogin(eventAccount.getAccount().getLogin());
                    member.setImportant(eventAccount.isImportant());
                    event.getAccountsDTOGets().add(member);
                }
                }
            List<EventExternalAccounts> eventExtAccounts=eventRepository.findOne(event.getId()).getMembersExternalAccounts();
            if (eventExtAccounts!=null)
                for (EventExternalAccounts eventAccount:eventExtAccounts){
                    EventAccountsDTOGet member=new EventAccountsDTOGet();
                    member.setExtSystem(eventAccount.getExternalSystem());
                    member.setStatus(EventMembersStatus.valueOf(eventAccount.getStatus()));
                    member.setLogin(eventAccount.getExternalEmail());
                    member.setImportant(eventAccount.isImportant());
                    event.getAccountsDTOGets().add(member);
                }
            //event.setStatus(EventMembersStatus.valueOf(obj.get("is_accepted").toString()));

            events.add(event);
        }
        return events;
    }

    public ResponseEntity updateEvent(int accountId, EventDTOPost eventToUpdate, int eventId) {
        Event oldEvent = eventRepository.findOne(eventId);
        if (oldEvent == null)
            return ResponseEntity.notFound().build();//no
        EventAccounts me = eventAccountsRepository.findByEventAndAccount(oldEvent, accountService.getAccountById(accountId));
        if (me != null)
            if (!me.getStatus().equals(EventMembersStatus.creator.toString()))
                return ResponseEntity.notFound().build();
        List<CalendarEvent> calendarEvents = calendarEventRepository.findCalendarEventByEvent(oldEvent);
        List<Integer> newCalendars = eventToUpdate.getCalendars();
        if (calendarEvents != null)
            for (CalendarEvent calendarEvent : calendarEvents) {
            if (calendarEvent.getCalendar().getAccount().getId().equals(accountId))
                if (newCalendars.contains(calendarEvent.getCalendar().getId())) {
                    if (calendarEvent.getExternalEventId() != null)
                        calendarIntService.updateEvent(calendarEvent.getCalendar(), eventToUpdate, calendarEvent.getExternalEventId());
                } else {
                    if (calendarEvent.getExternalEventId() != null)
                        calendarIntService.deleteEvent(calendarEvent.getCalendar(), calendarEvent.getExternalEventId());
                    calendarEventRepository.delete(calendarEvent);
                }
            }
        for (Integer newCalendar : newCalendars) {
            if (calendarEventRepository.findCalendarEventByCalendarAndEvent(calendarRepository.findOne(newCalendar), oldEvent) == null) {
                CalendarEvent calendarEvent = new CalendarEvent();
                Calendar calendar = calendarRepository.findOne(newCalendar);
                calendarEvent.setCalendar(calendar);
                calendarEvent.setEvent(oldEvent);
                if (calendar.getExternalId() != null) {
                    String extId = calendarIntService.createEvent(eventToUpdate, calendar);
                    if (extId != null)
                        calendarEvent.setExternalEventId(extId);
                    else throw new OutlookException("Not created in outlook external calendar");
                }
                calendarEventRepository.save(calendarEvent);
            }
        }
        LocalDateTime datebeg = eventToUpdate.getDateTimeBegin();
        LocalDateTime dateend = eventToUpdate.getDateTimeEnd();
        String zone = eventToUpdate.getTimeZone();
        ZonedDateTime utcZonedDateBegin = ZonedDateTime.of(datebeg, ZoneId.of(zone)).withZoneSameInstant(ZoneId.of("UTC"));
        ZonedDateTime utcZonedDateEnd = ZonedDateTime.of(dateend, ZoneId.of(zone)).withZoneSameInstant(ZoneId.of("UTC"));
        LocalDateTime utcDate = utcZonedDateBegin.toLocalDateTime();
        oldEvent.setDateTimeBegin(utcDate);
        long dur = utcZonedDateEnd.toInstant().toEpochMilli() - utcZonedDateBegin.toInstant().toEpochMilli();
        oldEvent.setDuration(dur);
        oldEvent.setDescription(eventToUpdate.getDescription());
        oldEvent.setName(eventToUpdate.getName());
        oldEvent.setAddress(eventToUpdate.getAddress());
        oldEvent.setRecurrence(eventToUpdate.getRecurrence());
        eventRepository.save(oldEvent);
        //List<>
        return ResponseEntity.ok().build();
    }

    public ResponseEntity cancelEvent(int accountId, int eventId, String extEventId, int calendarId) {
        Event event = eventRepository.findOne(eventId);

        if (event != null) {
            EventAccounts eventAccountMe = eventAccountsRepository.findByEventAndAccount(event, accountService.getAccountById(accountId));
            log.info("!!!!!!!!!{}", eventAccountMe.getId());
            if (eventAccountMe != null)
                if (eventAccountMe.getStatus().equals(EventMembersStatus.creator.toString())) {
                    event.setCancelled(true);
                    eventRepository.save(event);
                    for (CalendarEvent calendarEvent : event.getCalendarEvents()) {
                        if (calendarEvent.getExternalEventId() != null)
                            calendarIntService.deleteEvent(calendarEvent.getCalendar(), calendarEvent.getExternalEventId());
                    }
                    /*for (EventAccounts eventAccount : event.getMembersAccounts())
                        if (!eventAccount.getStatus().equals(EventMembersStatus.creator.toString()) && !eventAccount.getStatus().equals(EventMembersStatus.declined.toString()))
                            mail.sendLetter(eventAccount.getAccount().getEmail(), "TimeOn", "Событие " + event.getName() + " отменено ");
*/                }
        } else {
            List<CalendarEvent> calendarEvents = calendarEventRepository.findCalendarEventByExternalEventId(extEventId);
            if (calendarEvents != null)
                if (calendarEvents.size() > 0)
                    for (CalendarEvent calEvent : calendarEvents) {
                        calendarIntService.deleteEvent(calEvent.getCalendar(), calEvent.getExternalEventId());
                        calendarEventRepository.delete(calEvent);
                    }
                else calendarIntService.deleteEvent(calendarRepository.findOne(calendarId), extEventId);
        }
        return ResponseEntity.ok().build();
    }


    public List<EventDTOGetMy> getEventsOnStatus(int myId, String timeZone, EventMembersStatus status, int countOfRecords, int pageNumber) {

        List<EventDTOGetMy> events = new ArrayList<>();
        List<Map<String, Object>> eventList = jdbcTemplate.query(
                "SELECT events.id, events.name, events.start_time," +
                        " events.start_time + make_interval(0,0,0,0,0,0,cast(events.duration / 1000 as int)) as end_date, ea.status " +
                        " FROM " +
                        "  (SELECT e.*, CASE WHEN e.recurrence is not null " +
                        "       THEN unnest(get_occurrences(cast(e.recurrence as rrule), cast(e.date_time_begin as timestamp))) " +
                        "       ELSE e.date_time_begin END as start_time " +
                        "   FROM event e WHERE e.is_cancelled=false ) events " +
                        " JOIN event_accounts ea on " +
                        " ea.account_id=? and events.id=ea.event_id and ea.status = ? " +
                        " ORDER BY events.creation_date_time desc " +
                        ";"
                , new Object[]{myId, status.toString()},
                (rs, rowNum) -> ImmutableMap.of(
                        "id", rs.getInt(1),
                        "name", rs.getString(2),
                        "start_time", rs.getString(3),
                        "end_date", rs.getString(4),
                        "is_accepted", rs.getString(5)
                ));
        for (Map<String, Object> obj : eventList) {

            EventDTOGetMy event = new EventDTOGetMy();
            String begin = obj.get("start_time").toString(),
                    end = obj.get("end_date").toString();
            String tmp[] = begin.split(" ");
            begin = tmp[0].concat("T").concat(tmp[1]);
            tmp = end.split(" ");
            end = tmp[0].concat("T").concat(tmp[1]);
            begin = ZonedDateTime.of(LocalDateTime.parse(begin), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();
            end = ZonedDateTime.of(LocalDateTime.parse(end), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();

            event.setDateTimeBegin(begin.substring(0, 16));
            event.setDateTimeEnd(end.substring(0, 16));
            event.setName(obj.get("name").toString());
            event.setId((int) obj.get("id"));
            event.setStatus(EventMembersStatus.valueOf(obj.get("is_accepted").toString()));

            events.add(event);
        }
        return events;
    }


    public List<EventDTOGetMy> getMyCalendarEvents(LocalDateTime fromDate, LocalDateTime toDate, String timeZone,
                                                   List<Integer> calendarIds) {
        LocalDateTime utcFromDate = ZonedDateTime.of(fromDate, ZoneId.of(timeZone))
                .withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
        LocalDateTime utcToDate = ZonedDateTime.of(toDate, ZoneId.of(timeZone))
                .withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
        List<EventDTOGetMy> events = new ArrayList<>();
        for (Integer calendarId : calendarIds) {
            Calendar calendar = calendarRepository.findOne(calendarId);
            int myId = calendar.getAccount().getId();
            if (calendar.getExternalId() == null) {
                List<Map<String, Object>> eventList = jdbcTemplate.query(
                        "SELECT events.id, events.name, events.start_time," +
                                "  events.start_time + make_interval(0,0,0,0,0,0,cast(events.duration / 1000 as int)) as end_date, ea.status" +
                                " FROM " +
                                "  (SELECT e.*, CASE WHEN e.recurrence is not null " +
                                "       THEN unnest(get_occurrences(cast(e.recurrence as rrule), cast(e.date_time_begin as timestamp))) " +
                                "       ELSE e.date_time_begin END as start_time " +
                                "   FROM event e WHERE e.date_time_begin < cast(? as timestamp) AND e.is_cancelled=false) events " +
                                " JOIN event_accounts ea on " +
                                " ea.account_id=? and events.id=ea.event_id and ea.status <> 'declined' " +
                                " WHERE events.id IN (SELECT ce.event_id FROM calendar_event ce WHERE ce.calendar_id= ? )" +
                                " AND cast(events.start_time + make_interval(0,0,0,0,0,0,cast(events.duration / 1000 as int)) as timestamp) > cast(? as timestamp) " +
                                ";"
                        , new Object[]{utcToDate.toString(), myId, calendarId, utcFromDate.toString()},
                        (rs, rowNum) -> ImmutableMap.of(
                                "id", rs.getInt(1),
                                "name", rs.getString(2),
                                "start_time", rs.getString(3),
                                "end_date", rs.getString(4),
                                "is_accepted", rs.getString(5)
                        ));
                for (Map<String, Object> obj : eventList) {
                    EventDTOGetMy event = new EventDTOGetMy();
                    String begin = obj.get("start_time").toString(),
                            end = obj.get("end_date").toString();
                    String tmp[] = begin.split(" ");
                    begin = tmp[0].concat("T").concat(tmp[1]);
                    tmp = end.split(" ");
                    end = tmp[0].concat("T").concat(tmp[1]);
                    begin = ZonedDateTime.of(LocalDateTime.parse(begin), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();
                    end = ZonedDateTime.of(LocalDateTime.parse(end), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();

                    event.setDateTimeBegin(begin.substring(0, 16));
                    event.setDateTimeEnd(end.substring(0, 16));
                    event.setName(obj.get("name").toString());
                    event.setId((int) obj.get("id"));
                    event.setStatus(EventMembersStatus.valueOf(obj.get("is_accepted").toString()));
                    if (event.getCalendars() == null)
                        event.setCalendars(new ArrayList<>());
                    event.getCalendars().add(calendarId);
                    events.add(event);
                }
            } else {
                List<EventDTOGetMy> newList = calendarIntService.getMyEvents(calendarRepository.findOne(calendarId),
                        fromDate, toDate, timeZone);
                for (EventDTOGetMy getMy : newList) {
                    CalendarEvent extEvent=calendarEventRepository.findCalendarEventByCalendarAndExternalEventId(calendarRepository.findOne(calendarId), getMy.getExt_id());
                    if (extEvent!=null)
                        if (extEvent.getEvent()!=null)
                        {
                            Event oldEvent=extEvent.getEvent();
                            LocalDateTime dateTimeBegin=ZonedDateTime.of(oldEvent.getDateTimeBegin(),
                                    ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime();//.toString();
                            LocalDateTime dateTimeEnd=ZonedDateTime.of(oldEvent.getDateTimeBegin().plusSeconds(oldEvent.getDuration()/1000),
                                    ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime();//.toString();
                            if (!LocalDateTime.parse(getMy.getDateTimeBegin()).equals(dateTimeBegin) || !LocalDateTime.parse(getMy.getDateTimeEnd()).equals(dateTimeEnd) ||
                                    !getMy.getName().equals(oldEvent.getName()))
                            {
                                Event newEvent=new Event();
                                MyEventJson updatedEvent=calendarIntService.getEventById(calendarRepository.findOne(calendarId), extEvent.getExternalEventId(),timeZone);
                                ZonedDateTime begin=ZonedDateTime.of(LocalDateTime.parse(updatedEvent.getStart().getDateTime().toString()), ZoneId.of(timeZone) ).withZoneSameInstant(ZoneId.of("UTC"));
                                ZonedDateTime end=ZonedDateTime.of(LocalDateTime.parse(updatedEvent.getEnd().getDateTime().toString()), ZoneId.of(timeZone) ).withZoneSameInstant(ZoneId.of("UTC"));

                                newEvent.setDateTimeBegin(begin.toLocalDateTime());
                                newEvent.setDuration(end.toInstant().toEpochMilli()-begin.toInstant().toEpochMilli());
                                newEvent.setAddress(updatedEvent.getLocation().getDisplayName());
                                newEvent.setCreationDataTime(LocalDateTime.now());
                                newEvent.setName(updatedEvent.getSubject());
                                newEvent.setDescription(updatedEvent.getBodyPreview());
                                newEvent.setCancelled(false);

                                //newEvent.setMembersExternalAccounts(updatedEvent.getAttendees().);
                                if (oldEvent.getCalendarEvents().size()==1)
                                    newEvent.setId(oldEvent.getId());
                                eventRepository.save(newEvent);
                                externalAccountsRepository.delete(oldEvent.getMembersExternalAccounts());
                                //if (oldEvent.getMembersExternalAccounts())
                                if (updatedEvent.getAttendees()!=null)
                                    for (Attendee attendee:updatedEvent.getAttendees())
                                    {
                                        EventExternalAccounts externalAccount=new EventExternalAccounts();
                                        externalAccount.setExternalSystem(extEvent.getCalendar().getExternalAccount().getExternalSystem());
                                        if (attendee.getType().equals("required"))
                                            externalAccount.setImportant(true);
                                        externalAccount.setExternalEmail(attendee.getEmailAddress().getAddress());
                                        externalAccount.setEvent(newEvent);
                                        externalAccount.setStatus("none");
                                        externalAccountsRepository.save(externalAccount);
                                    }
                                extEvent.setEvent(newEvent);
                                calendarEventRepository.save(extEvent);
                            }
                        }
                    if (getMy.getCalendars() == null)
                        getMy.setCalendars(new ArrayList<>());
                    getMy.getCalendars().add(calendarId);
                }
                events.addAll(newList);
            }

        }

        for (EventDTOGetMy dtoGetMy : events) {
            String id = dtoGetMy.getExt_id();
            if (id != null) {
                List<CalendarEvent> calendarEvents = calendarEventRepository.findCalendarEventByExternalEventId(id);
                if (calendarEvents != null) {
                    for (CalendarEvent calendarEvent : calendarEvents) {
                        if (calendarEvent.getEvent() != null) {
                            int eventId = calendarEvent.getEvent().getId();
                            for (EventDTOGetMy dtoGetMy1 : events)
                                if (eventId == dtoGetMy1.getId()) {
                                    dtoGetMy1.getCalendars().add(calendarEvent.getCalendar().getId());
                                    dtoGetMy = null;
                                }
                            if (dtoGetMy!=null)
                                if (dtoGetMy.getId()==0)
                                    dtoGetMy.setId(eventId);
                        } else {
                            if (dtoGetMy.getCalendars() == null)
                                dtoGetMy.setCalendars(new ArrayList<>());
                            if (!dtoGetMy.getCalendars().contains(calendarEvent.getCalendar().getId()))
                                dtoGetMy.getCalendars().add(calendarEvent.getCalendar().getId());
                        }
                    }

                }

            }

        }

        return events;
    }

    public List<EventDTOGetNotMy> getOthersEvents(String login, LocalDateTime fromDate,
                                                  LocalDateTime toDate, String timeZone, int me) {
        LocalDateTime utcFromDate = ZonedDateTime.of(fromDate, ZoneId.of(timeZone))
                .withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
        LocalDateTime utcToDate = ZonedDateTime.of(toDate, ZoneId.of(timeZone))
                .withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
        List<EventDTOGetNotMy> events = new ArrayList<>();

        Account user = accountService.findByLogin(login);

        boolean flag = false, flag2 = false;
        if (user == null) {
            flag2 = true;
            user = accountService.searchByExternalLogin(login);
            if (user != null)
                if (user.isVisible())
                    flag = true;

        }

        if (user != null || (flag && flag2)) {

            List<Calendar> calendars = user.getCalendars();//calendarRepository.findCalendarByAccount(user);
            for (Calendar calendar : calendars) {
                //Calendar calendar=calendarRepository.findOne(calendarDTO.getId());
                if (calendar.getExternalId() == null) {
                    log.info(calendar.getId().toString());
                    List<Map<String, Object>> eventList = jdbcTemplate.query(
                            "SELECT events.id, events.start_time, "+
" events.start_time + make_interval(0,0,0,0,0,0,cast(events.duration / 1000 as int)) as end_date "+
 "                   FROM "+
  "                          (SELECT e.*, CASE WHEN e.recurrence is not null "+
   "                 THEN unnest(get_occurrences(cast(e.recurrence as rrule), cast(e.date_time_begin as timestamp))) "+
    "                ELSE e.date_time_begin END as start_time "+
     "              FROM event e WHERE e.date_time_begin < cast(? as timestamp) AND e.is_cancelled=false) events "+
      "              JOIN event_accounts ea on "+
       "             ea.account_id=? and events.id=ea.event_id and (ea.status ='accepted' or ea.status='creator') "+
        "    WHERE events.id IN (SELECT ce.event_id FROM calendar_event ce WHERE ce.calendar_id= ? ) "+
          "          AND cast(events.start_time + make_interval(0,0,0,0,0,0,cast(events.duration / 1000 as int)) as timestamp) > cast(? as timestamp) "+
           "         ;", new Object[]{utcToDate.toString(), user.getId(), calendar.getId(), utcFromDate.toString()},
                            (rs, rowNum) -> ImmutableMap.of(
                                    "id", rs.getInt(1),
                                    "start_time", rs.getString(2),
                                    "end_date", rs.getString(3)
                            ));
                    for (Map<String, Object> obj : eventList) {
                        EventDTOGetNotMy event = new EventDTOGetNotMy();
                        String begin = obj.get("start_time").toString(),
                                end = obj.get("end_date").toString();
                        String tmp[] = begin.split(" ");
                        begin = tmp[0].concat("T").concat(tmp[1]);
                        tmp = end.split(" ");
                        end = tmp[0].concat("T").concat(tmp[1]);

                        begin = ZonedDateTime.of(LocalDateTime.parse(begin), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();
                        end = ZonedDateTime.of(LocalDateTime.parse(end), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone)).toLocalDateTime().toString();
                        event.setDateTimeBegin(begin.substring(0, 16));
                        event.setDateTimeEnd(end.substring(0, 16));
                        events.add(event);
                    }
                }
            }

            if (user.getExternalAccounts() != null) {
                List<ExternalAccount> myExtAccounts = externalAccountRepository.findExternalAccountsByAccount
                        (accountService.getAccountById(me));

                for (ExternalAccount externalAccount : user.getExternalAccounts()) {
                    for (ExternalAccount myExtAcc : myExtAccounts) {
                        if (myExtAcc.getExternalSystem().equals(externalAccount.getExternalSystem())) {
                            List<CalendarDTOGet> calendarDTOGets = calendarIntService.getSharedCalendars(myExtAcc.getId(), externalAccount.getUserEmail());
                            if (calendarDTOGets != null)
                                for (CalendarDTOGet cal : calendarDTOGets) {

                                    events.addAll(calendarIntService.getUserEvents(cal.getId(), fromDate, toDate, timeZone, myExtAcc));
                                }
                        }
                    }
                }
            }

        }
        //}
        else {

            List<ExternalAccount> myExtAccounts = externalAccountRepository.findExternalAccountsByAccount
                    (accountService.getAccountById(me));
            for (ExternalAccount myExtAcc : myExtAccounts) {
                List<CalendarDTOGet> calendarDTOGets = calendarIntService.getSharedCalendars(myExtAcc.getId(), login);
                if (calendarDTOGets != null)
                    for (CalendarDTOGet cal : calendarDTOGets) {

                        events.addAll(calendarIntService.getUserEvents(cal.getId(), fromDate, toDate, timeZone, myExtAcc));
                    }
            }

        }
        return events;
    }




    public EventDTOGet getEvent(int accountId, int eventId, String extEventId, int calendarId, String timeZone){
        EventDTOGet eventDTOPost=new EventDTOGet();
        eventDTOPost.setCalendars(new ArrayList<>());
        eventDTOPost.setMembersAccounts(new ArrayList<>());
        eventDTOPost.setMembersGroups(new ArrayList<>());
        if (eventRepository.findOne(eventId)!=null) {

            Event event=eventRepository.findOne(eventId);
            eventDTOPost.setAddress(event.getAddress());
            eventDTOPost.setDateTimeBegin(ZonedDateTime.of(event.getDateTimeBegin(), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone )).toLocalDateTime().toString());
            eventDTOPost.setDateTimeEnd(ZonedDateTime.of(event.getDateTimeBegin().plusSeconds(event.getDuration()/1000), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone )).toLocalDateTime().toString());
            eventDTOPost.setDescription(event.getDescription());
            eventDTOPost.setName(event.getName());
            eventDTOPost.setRecurrence(event.getRecurrence());
            List<CalendarEvent> calendars=event.getCalendarEvents();

            for (CalendarEvent calendarEvent:calendars){
                if (!eventDTOPost.getCalendars().contains(calendarEvent.getCalendar().getId()))
                    eventDTOPost.getCalendars().add(calendarEvent.getCalendar().getId());
            }
            for (EventAccounts eventAccount:event.getMembersAccounts()){
                EventAccountsDTOGet eventAccountsDTOPost=new EventAccountsDTOGet();
                eventAccountsDTOPost.setExtSystem(null);
                eventAccountsDTOPost.setImportant(eventAccount.isImportant());
                eventAccountsDTOPost.setLogin(eventAccount.getAccount().getLogin());
                //eventAccountsDTOPost.setName(eventAccount.getAccount().getName());
                eventAccountsDTOPost.setStatus(EventMembersStatus.valueOf(eventAccount.getStatus()));
                eventDTOPost.getMembersAccounts().add(eventAccountsDTOPost);
            }
            for (EventExternalAccounts externalAccount:event.getMembersExternalAccounts()){
                EventAccountsDTOGet eventAccountsDTOPost=new EventAccountsDTOGet();
                eventAccountsDTOPost.setExtSystem(externalAccount.getExternalSystem());
                eventAccountsDTOPost.setImportant(externalAccount.isImportant());
                eventAccountsDTOPost.setLogin(externalAccount.getExternalEmail());
                eventAccountsDTOPost.setStatus(EventMembersStatus.valueOf(externalAccount.getStatus()));
                //eventAccountsDTOPost.setName(eventAccount.getAccount().getName());
                eventDTOPost.getMembersAccounts().add(eventAccountsDTOPost);
            }
            for (EventGroups eventGroup:event.getMembersGroups()){
                EventGroupDTOGet eventGroupsDTOPost=new EventGroupDTOGet();
                eventGroupsDTOPost.setGroup(eventGroup.getId());
                eventGroupsDTOPost.setImportant(eventGroup.isImportant());
                eventGroupsDTOPost.setName(eventGroup.getGroup().getName());
                eventDTOPost.getMembersGroups().add(eventGroupsDTOPost);
            }

        }
        else if (calendarRepository.findOne(calendarId)!=null && calendarRepository.findOne(calendarId).getExternalAccount()!=null){
            MyEventJson event=calendarIntService.getEventById(calendarRepository.findOne(calendarId), extEventId, timeZone);
            eventDTOPost.setAddress(event.getLocation().getDisplayName());
            eventDTOPost.setDateTimeBegin(ZonedDateTime.of(LocalDateTime.parse(event.getStart().getDateTime()), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone )).toLocalDateTime().toString());
            eventDTOPost.setDateTimeEnd(ZonedDateTime.of(LocalDateTime.parse(event.getEnd().getDateTime()), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of(timeZone )).toLocalDateTime().toString());
            eventDTOPost.setDescription(event.getBodyPreview());
            eventDTOPost.setName(event.getSubject());
            eventDTOPost.setRecurrence(null);
            //List<CalendarEvent> calendars=event.getCalendarEvents();

            //for (CalendarEvent calendarEvent:calendars){
              //  if (!eventDTOPost.getCalendars().contains(calendarEvent.getCalendar().getId()))
                    eventDTOPost.getCalendars().add(calendarId);
           // }
            for (Attendee eventAccount:event.getAttendees()){
                EventAccountsDTOGet eventAccountsDTOPost=new EventAccountsDTOGet();
                eventAccountsDTOPost.setExtSystem(calendarRepository.findOne(calendarId).getExternalAccount().getExternalSystem());
                if (eventAccount.getType().equals("required"))
                    eventAccountsDTOPost.setImportant(true);
                else                     eventAccountsDTOPost.setImportant(false);

                eventAccountsDTOPost.setLogin(eventAccount.getEmailAddress().getAddress());
                //eventAccountsDTOPost.setName(eventAccount.getAccount().getName());
                eventAccountsDTOPost.setStatus(EventMembersStatus.none);
                eventDTOPost.getMembersAccounts().add(eventAccountsDTOPost);
            }
        }
        return eventDTOPost;
    }


    public int createEvent(int accountId, EventDTOPost eventDTO) {
        Account account = accountService.getAccountById(accountId);
        List<Integer> calendarsId = eventDTO.getCalendars();
        boolean outlookFlag = false, googleFlag = false;
        for (Integer calendar : calendarsId) {
            if (calendarRepository.findOne(calendar).getExternalAccount() != null)
                if (calendarRepository.findOne(calendar).getExternalAccount().getExternalSystem().equals(ExternalSystem.OUTLOOK.toString()))
                    outlookFlag = true;
                else
                    googleFlag = true;
        }
        int res = 0;
        Event eventToCreate = eventDTOPostMapper.eventDTOPostToEvent(eventDTO);
        eventToCreate.setCancelled(false);
        LocalDateTime datebeg = eventDTO.getDateTimeBegin();
        LocalDateTime dateend = eventDTO.getDateTimeEnd();
        String zone = eventDTO.getTimeZone();
        ZonedDateTime utcZonedDateBegin = ZonedDateTime.of(datebeg, ZoneId.of(zone)).withZoneSameInstant(ZoneId.of("UTC"));
        ZonedDateTime utcZonedDateEnd = ZonedDateTime.of(dateend, ZoneId.of(zone)).withZoneSameInstant(ZoneId.of("UTC"));
        LocalDateTime utcDate = utcZonedDateBegin.toLocalDateTime();
        eventToCreate.setDateTimeBegin(utcDate);
        eventToCreate.setCreationDataTime(LocalDateTime.now());
        long dur = utcZonedDateEnd.toInstant().toEpochMilli() - utcZonedDateBegin.toInstant().toEpochMilli();
        eventToCreate.setDuration(dur);
        if (calendarsId.size() > 1 ||
                (calendarsId.size() == 1 && calendarRepository.findOne(calendarsId.get(0)).getExternalId() == null)) {
            eventRepository.save(eventToCreate);
            res = eventToCreate.getId();
            for (int cal : calendarsId) {
                CalendarEvent calendarEvent = new CalendarEvent();
                Calendar calendar = calendarRepository.findOne(cal);
                calendarEvent.setCalendar(calendar);
                calendarEvent.setEvent(eventToCreate);
                if (calendar.getExternalId() != null) {
                    String extId = calendarIntService.createEvent(eventDTO, calendar);
                    if (extId != null)
                        calendarEvent.setExternalEventId(extId);
                    else throw new OutlookException("Not created in outlook external calendar");
                }
                calendarEventRepository.save(calendarEvent);
            }
            EventAccounts eventAccounts2 = new EventAccounts();
            eventAccounts2.setAccount(accountService.getAccountById(accountId));
            eventAccounts2.setEvent(eventToCreate);
            eventAccounts2.setImportant(true);
            eventAccounts2.setStatus(EventMembersStatus.creator.toString());
            eventAccountsRepository.save(eventAccounts2);
            List<EventAccountsDTOPost> eventAccountsDTO = eventDTO.getMembersAccounts();
            if (eventAccountsDTO != null) {
                for (EventAccountsDTOPost membersAccount : eventAccountsDTO) {
                    if (membersAccount.getExtSystem() == null) {
                        EventAccounts eventAccounts = new EventAccounts();
                        Account account1 = accountService.findByLogin(membersAccount.getLogin());
                        /*if (account1==null){
                            account1=accountService.searchByExternalLogin(membersAccount.getLogin());
                            if (account1!=null)
                                if (account1.isVisible())
                        }*/
                        if (account1 != null) {
                            if (eventAccountsRepository.findByEventAndAccount(eventToCreate, account1) == null) {
                                eventAccounts.setAccount(account1);
                                eventAccounts.setEvent(eventToCreate);
                                eventAccounts.setImportant(membersAccount.isImportant());
                                eventAccounts.setStatus(EventMembersStatus.none.toString());
                                List<Calendar> calendars = account1.getCalendars();
                                for (Calendar calendar : calendars) {
                                    if (calendar.getExternalAccount() == null) {
                                        CalendarEvent calendarEvent = new CalendarEvent();
                                        calendarEvent.setCalendar(calendar);
                                        calendarEvent.setEvent(eventToCreate);
                                        calendarEventRepository.save(calendarEvent);
                                    }
                                }

                                    eventAccountsRepository.save(eventAccounts);
                                    //mail.sendLetter(account1.getEmail(), "TimeOn", "Приглашение на встречу " + eventToCreate.getName()
                                    //        + " от " + eventAccounts2.getAccount().getLogin());


                            }
                        }
                    } else {
                        EventExternalAccounts eventExternalAccounts = new EventExternalAccounts();
                        eventExternalAccounts.setEvent(eventToCreate);
                        eventExternalAccounts.setExternalEmail(membersAccount.getLogin());
                        eventExternalAccounts.setExternalSystem(membersAccount.getExtSystem());
                        eventExternalAccounts.setImportant(membersAccount.isImportant());
                        eventExternalAccounts.setStatus(EventMembersStatus.none.toString());
                        externalAccountsRepository.save(eventExternalAccounts);
                        /*if (eventExternalAccounts.getExternalSystem().equals(ExternalSystem.OUTLOOK.toString()) && !outlookFlag ||
                                eventExternalAccounts.getExternalSystem().equals(ExternalSystem.GOOGLE.toString()) && !googleFlag)
                            mail.sendLetter(membersAccount.getLogin(), "TimeOn", "Приглашение на встречу " + eventToCreate.getName()
                                    + " от " + eventAccounts2.getAccount().getLogin());
*/

                    }
                }
            }
            List<EventGroupsDTOPost> eventGroupsDTO = eventDTO.getMembersGroups();
            if (eventGroupsDTO != null) {
                for (EventGroupsDTOPost membersGroup : eventGroupsDTO) {
                    EventGroups eventGroups = new EventGroups();
                    GroupPeople group = groupPeopleRepository.findOne(membersGroup.getGroup());
                    eventGroups.setEvent(eventToCreate);
                    eventGroups.setGroup(group);
                    eventGroups.setImportant(membersGroup.isImportant());
                    for (Account account1 : group.getMembers()) {
                        if (eventAccountsRepository.findByEventAndAccount(eventToCreate, account1) == null) {
                            EventAccounts eventAccounts = new EventAccounts();
                            eventAccounts.setAccount(account1);
                            eventAccounts.setEvent(eventToCreate);
                            eventAccounts.setImportant(membersGroup.isImportant());
                            eventAccounts.setStatus(EventMembersStatus.none.toString());
                            List<Calendar> calendars = account1.getCalendars();
                            for (Calendar calendar : calendars) {
                                if (calendar.getExternalAccount() == null) {
                                    CalendarEvent calendarEvent = new CalendarEvent();
                                    calendarEvent.setCalendar(calendar);
                                    calendarEvent.setEvent(eventToCreate);
                                    calendarEventRepository.save(calendarEvent);
                                }
                            }
                            eventAccountsRepository.save(eventAccounts);

                            //mail.sendLetter(account1.getEmail(), "TimeOn", "Приглашение на встречу " + eventToCreate.getName()
                              //      + " от " + eventAccounts2.getAccount().getLogin());


                        }

                    }
                    eventGroupsRepository.save(eventGroups);
                }
            }

        } else {
            CalendarEvent calendarEvent = new CalendarEvent();
            Calendar calendar = calendarRepository.findOne(calendarsId.get(0));
            calendarEvent.setCalendar(calendar);
            calendarEvent.setExternalEventId(calendarIntService.createEvent(eventDTO, calendar));
            calendarEventRepository.save(calendarEvent);
            res = calendarEvent.getId();
        }
        return res;
    }


    public ResponseEntity changeEventStatus(int myId, EventMembersStatus status, int eventId) {
        EventAccounts me = eventAccountsRepository.findByEventAndAccount(eventRepository.findOne(eventId), accountService.getAccountById(myId));
        if (me != null) {
            me.setStatus(status.toString());
            eventAccountsRepository.save(me);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }


}
