package nceductimetable.demo.services;

import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.calendarSystem.CalendarSystem;
import nceductimetable.demo.calendarSystem.MyEventJson;
import nceductimetable.demo.calendarSystem.OutlookCalendar;
import nceductimetable.demo.calendarSystem.GoogleCalendar;
import nceductimetable.demo.dtoGet.CalendarDTOGet;
import nceductimetable.demo.dtoGet.EventDTOGetMy;
import nceductimetable.demo.dtoGet.EventDTOGetNotMy;
import nceductimetable.demo.dtoOutlook.EventOutlookDTOPost;
import nceductimetable.demo.dtoOutlook.Recurrence;
import nceductimetable.demo.dtoPost.EventDTOPost;
import nceductimetable.demo.enumForEvents.ExternalSystem;
import nceductimetable.demo.mappers.OutlookEventMapper;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.model.Calendar;
import nceductimetable.demo.model.ExternalAccount;
import nceductimetable.demo.repositories.ExternalAccountRepository;
import org.codehaus.jackson.map.ObjectMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CalendarIntService {
    private final ExternalAccountRepository externalAccountRepository;
    Map<ExternalSystem, CalendarSystem> calendarSystemMap=new HashMap<>();

    public CalendarIntService(ExternalAccountRepository externalAccountRepository){
        this.externalAccountRepository=externalAccountRepository;
    }
    @PostConstruct
    public void registerExtSystems(){
        calendarSystemMap.put(ExternalSystem.OUTLOOK, new OutlookCalendar(externalAccountRepository));
        calendarSystemMap.put(ExternalSystem.GOOGLE, new GoogleCalendar(externalAccountRepository));
    }

    public String createEvent(EventDTOPost eventDTOPost, Calendar calendar){
        String externalSystem=calendar.getExternalAccount().getExternalSystem();
        return calendarSystemMap.get(ExternalSystem.valueOf(externalSystem)).createEvent(calendar, eventDTOPost);
    }

    public List<EventDTOGetMy> getMyEvents(Calendar calendar, LocalDateTime begin, LocalDateTime end, String timeZone){
        return calendarSystemMap.get(ExternalSystem.valueOf(calendar.getExternalAccount().getExternalSystem())).
                getMyCalendarEvents(calendar, begin,end, timeZone);

    }

    public List<EventDTOGetNotMy> getUserEvents(String extCalendarId, LocalDateTime begin,
                                                LocalDateTime end, String timeZone, ExternalAccount my){
        return calendarSystemMap.get(ExternalSystem.valueOf(my.getExternalSystem())).
                getUserEvents(extCalendarId, begin,end, timeZone, my);
    }

    public List<CalendarDTOGet> getMyNotConnectedCalendars(int ext_accountId){
        ExternalAccount externalAccount=externalAccountRepository.findOne(ext_accountId);
        //log.error("{} external account id", externalAccount.getId());
        //log.error("{} external account system", externalAccount.getExternalSystem());
        return calendarSystemMap.get(ExternalSystem.valueOf(externalAccount.getExternalSystem())).getMyNotConnectedCalendars(externalAccount);
    }

    public List<CalendarDTOGet> getSharedCalendars(int ext_accountId, String login){
        ExternalAccount externalAccount=externalAccountRepository.findOne(ext_accountId);
        //log.error("{} external account id", externalAccount.getId());
        //log.error("{} external account system", externalAccount.getExternalSystem());
        return calendarSystemMap.get(ExternalSystem.valueOf(externalAccount.getExternalSystem())).getMySharedCalendars(externalAccount, login);
    }

    public void updateEvent(Calendar calendar, EventDTOPost eventDTOPost, String eventId){
        String externalSystem=calendar.getExternalAccount().getExternalSystem();
        calendarSystemMap.get(ExternalSystem.valueOf(externalSystem)).updateEvent(calendar, eventDTOPost, eventId);
    }

    public void deleteEvent(Calendar calendar, String eventId){
        String externalSystem=calendar.getExternalAccount().getExternalSystem();
        calendarSystemMap.get(ExternalSystem.valueOf(externalSystem)).deleteEvent(calendar, eventId);
    }

    public MyEventJson getEventById(Calendar calendar, String eventId, String timeZone){
        String externalSystem=calendar.getExternalAccount().getExternalSystem();
        return calendarSystemMap.get(ExternalSystem.valueOf(externalSystem)).getEventById(calendar, eventId, timeZone);

    }
}
