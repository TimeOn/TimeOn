package nceductimetable.demo.services;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.Hashing;
import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.auth.Role;
import nceductimetable.demo.dto.AccountAuth;
import nceductimetable.demo.dto.AccountLogin;
import nceductimetable.demo.dto.AccountRegister;
import nceductimetable.demo.exceptions.ActivateAccountException;
import nceductimetable.demo.exceptions.LoginDataException;
import nceductimetable.demo.exceptions.RegistrationDataException;
import nceductimetable.demo.mappers.AccountAuthMapper;
import nceductimetable.demo.mappers.AccountRegisterMapper;
import nceductimetable.demo.model.Account;
import nceductimetable.demo.repositories.UserRepository;
import org.apache.commons.lang.RandomStringUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

@Service
@Slf4j
public class UserService implements UserDetailsService {

    @Value("${server.activation.address}")
    private String adress;

    @Value("${security.salt}")
    private String salt;

    private AccountAuthMapper accountAuthMapper = Mappers.getMapper(AccountAuthMapper.class);
    private AccountRegisterMapper accountRegisterMapper = Mappers.getMapper(AccountRegisterMapper.class);
    private final UserRepository userRepository;
    private final Mail letter;
    private final TokenHandler tokenHandler;
    private final CalendarService calendarService;

    @Autowired
    public UserService(UserRepository userRepository, Mail letter, TokenHandler tokenHandler, CalendarService calendarService) {
        this.userRepository = userRepository;
        this.letter = letter;
        this.tokenHandler = tokenHandler;
        this.calendarService=calendarService;
    }

    public void createUser(AccountRegister accountDTO) {
        if (userRepository.getAccountByLogin(accountDTO.getLogin()).isPresent()) {
            log.error("{} login is already taken", accountDTO.getLogin());
            throw new RegistrationDataException(accountDTO.getLogin() + " login is already taken");
        } else if (userRepository.getAccountByMail(accountDTO.getEmail()).isPresent()) {
            log.error("{} email is already taken", accountDTO.getEmail());
            throw new RegistrationDataException(accountDTO.getEmail() + " email is already taken");
        } else {
            Account account = accountRegisterMapper.accountRegistertoAccount(accountDTO);
            String hashed = Hashing.sha512().hashString(accountDTO.getPassword() + salt, StandardCharsets.UTF_8).toString();
            account.setPassword(hashed);
            account.setCreationDate(LocalDateTime.now());
            String activation = RandomStringUtils.randomAlphanumeric(15);
            account.setActivate(activation);
            log.info("Try to save account\n");
            account.setVisible(true);
            //account.setTimezone(TimeZone.getDefault().getDisplayName());

            userRepository.save(account);

            calendarService.createCalendar(account, "default", null);

            letter.sendLetter(account.getEmail(), "Your new account", "You are receiving this e-mail because you requested a new" +
                    " account.\n\n" + adress + activation);
        }
    }

    public void activateUser(String code) {
        log.info("Check activation code {}\n", code);
        Account account = userRepository.getAccountByActivateCode(code)
                .orElseThrow(() -> new ActivateAccountException("There is no account with this activation code" + code));
        account.setActivated(true);
        account.setActivate(null);
        log.info("Activate account\n");
        userRepository.save(account);
    }

    public String loginUser(AccountLogin accountLogin) {
        try {
            Account account = userRepository.getAccountByLogin(accountLogin.getLogin())
                    .orElseThrow(() -> new UsernameNotFoundException("user " + accountLogin.getLogin() + " was not found!"));
            log.info("Try to generate accesee token\n");
            return tokenHandler.generateAccessToken(account.getEmail(), account.getLogin(), account.getPassword() + salt, LocalDateTime.now().plusDays(14));
        } catch (LoginDataException e) {
            throw new LoginDataException("Authentification error");
        }
    }

    public void testToken(String token) {
        log.info("Try to extract login\n");
        Optional<String> tok = tokenHandler.extractUserLogin(token);
        String temp = tok.orElseThrow(() -> new UsernameNotFoundException("user was not found!"));
        Account account = userRepository.getAccountByLogin(temp)
                .orElseThrow(() -> new UsernameNotFoundException("user was not found!"));
        letter.sendLetter(account.getEmail(), "TimeOn", "Hello " + account.getName() + " " + account.getSurname());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return findByLogin(username).orElse(null);
    }

    public Optional<AccountAuth> findByLogin(String login) {
        Account account = userRepository.getAccountByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("user " + login + " was not found!"));
        AccountAuth accountAuth = accountAuthMapper.accountToAccountAuth(account);
        accountAuth.setAuthorities(ImmutableList.of(Role.USER));
        accountAuth.setAccountNonExpired(true);
        accountAuth.setAccountNonLocked(true);
        accountAuth.setCredentialsNonExpired(true);
        return Optional.of(accountAuth);
    }

    public int getMyId(String token){
        Optional<String> tok = tokenHandler.extractUserLogin(token);
        String temp = tok.orElseThrow(() -> new UsernameNotFoundException("user was not found!"));
        Account account = userRepository.getAccountByLogin(temp).
                orElseThrow(() -> new UsernameNotFoundException("user was not found!"));
        return account.getId();
    }
}
