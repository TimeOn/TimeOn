package nceductimetable.demo.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import nceductimetable.demo.dto.AccountForGroups;
import nceductimetable.demo.dto.GroupPeopleDTO;
import nceductimetable.demo.dto.GroupPeopleDTOGet;
import nceductimetable.demo.exceptions.AcceptGroupException;
import nceductimetable.demo.mappers.GroupPeopleMapper;
import nceductimetable.demo.model.GroupPeople;
import nceductimetable.demo.repositories.AccountRepository;
import nceductimetable.demo.repositories.GroupPeopleRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class GroupPeopleService {

    private final GroupPeopleRepository groupPeopleRepository;
    private final AccountRepository accountRepository;
    private final TokenHandler tokenHandler;
    private final GroupPeopleMapper groupPeopleMapper = Mappers.getMapper(GroupPeopleMapper.class);

    @Autowired
    public GroupPeopleService(GroupPeopleRepository groupPeopleRepository, AccountRepository accountRepository, TokenHandler tokenHandler) {
        this.groupPeopleRepository = groupPeopleRepository;
        this.accountRepository = accountRepository;
        this.tokenHandler = tokenHandler;
    }

    public int createGroupPeople(String token, GroupPeopleDTO groupPeople) {
        GroupPeople savingGroupPeople = new GroupPeople();
        savingGroupPeople.setName(groupPeople.getName());
        savingGroupPeople.setDescription(groupPeople.getDescription());
        savingGroupPeople.setCreator(accountRepository.findAccountByLogin(tokenHandler.extractUserLogin(token).get()));
        savingGroupPeople.getMembers().add(accountRepository.findAccountByLogin(tokenHandler.extractUserLogin(token).get()));
        for(AccountForGroups accountForGroups:groupPeople.getMembers())
            savingGroupPeople.getMembers().add(accountRepository.findOne(accountForGroups.getId()));
        return groupPeopleRepository.save(savingGroupPeople).getId();
    }

    public GroupPeopleDTO getGroupPeopleById(int groupId) {
        return groupPeopleMapper.groupPeopleToGroupPeopleDTO(groupPeopleRepository.findOne(groupId));
    }

    public int updateGroupPeople(int groupId, GroupPeopleDTO newGroupPeople, String token) {
        GroupPeople savingGroupPeople = groupPeopleRepository.findOne(groupId);
        if (Objects.equals(savingGroupPeople.getCreator().getLogin(), tokenHandler.extractUserLogin(token).get())) {
            savingGroupPeople.setDescription(newGroupPeople.getDescription());
            savingGroupPeople.setName(newGroupPeople.getName());
            return groupPeopleRepository.save(savingGroupPeople).getId();
        }
        throw new AcceptGroupException("Only creator can to update group.");
    }

    public void deleteGroup(int groupId, String token) {
        if (Objects.equals(groupPeopleRepository.findOne(groupId).getCreator().getLogin(), tokenHandler.extractUserLogin(token).get())) {
            groupPeopleRepository.delete(groupId);
        }
        throw new AcceptGroupException("Only creator can to delete group.");
    }

    public int addMemberInGroup(int groupId, List<String> addedAccountLogins) {
        GroupPeople savingGroupPeople = groupPeopleRepository.findOne(groupId);
        for (String addedAccountLogin : addedAccountLogins) {
            savingGroupPeople.getMembers().add(accountRepository.findAccountByLogin(addedAccountLogin));
        }
        return groupPeopleRepository.save(savingGroupPeople).getId();
    }

    public int deleteMemberFromGroup(int groupId, List<String> deletedAccountLogins) {
        GroupPeople savingGroupPeople = groupPeopleRepository.findOne(groupId);
        for (String deletedAccountLogin : deletedAccountLogins) {
            savingGroupPeople.getMembers().remove(accountRepository.findAccountByLogin(deletedAccountLogin));
        }
        return groupPeopleRepository.save(savingGroupPeople).getId();
    }

    public List<GroupPeopleDTOGet> getMyGroups(String token){
        List<GroupPeople> groupPeople=groupPeopleRepository.findGroupPeopleByMembers(accountRepository.findAccountByLogin(tokenHandler.extractUserLogin(token).get()));
        List<GroupPeopleDTOGet> groupPeopleDTOS=new ArrayList<>();
        for (GroupPeople groupPeople1:groupPeople){
            groupPeopleDTOS.add(groupPeopleMapper.groupToDroupDTOGet(groupPeople1));
        }
        return groupPeopleDTOS;
    }

    public List<GroupPeopleDTOGet> getUserGroups(int id){
        List<GroupPeople> groupPeople=groupPeopleRepository.findGroupPeopleByMembers(accountRepository.findOne(id));
        List<GroupPeopleDTOGet> groupPeopleDTOS=new ArrayList<>();
        for (GroupPeople groupPeople1:groupPeople){
            groupPeopleDTOS.add(groupPeopleMapper.groupToDroupDTOGet(groupPeople1));
        }
        return groupPeopleDTOS;
    }

    public List<GroupPeopleDTOGet> searchGroup(String name){
        List<GroupPeople> groups=groupPeopleRepository.findByNameLikeIgnoreCase(name.concat("%"));
        List<GroupPeopleDTOGet> res=new ArrayList<>();
        for (GroupPeople group:groups){
            res.add(groupPeopleMapper.groupToDroupDTOGet(group));
        }
        return res;
    }
}
