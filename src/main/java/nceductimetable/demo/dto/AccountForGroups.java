package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
public class AccountForGroups {

    private int id;
    private String email;
    private String login;
    private String name;
    private String surname;
}
