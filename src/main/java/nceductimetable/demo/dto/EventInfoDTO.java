package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
@ToString
public class EventInfoDTO{
    private LocalDateTime dateTimeBegin;
    private LocalDateTime dateTimeEnd;
}
