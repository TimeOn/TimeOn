package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class AccountsEmployments {
    private LocalDateTime begin;
    private LocalDateTime end;
    private List<String> accounts;
}
