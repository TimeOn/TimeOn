package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class AccountForSearch {
    private int id;
    private String email;
    private String login;
    private String name;
    private String surname;
    //List<String> externalAccounts;
}
