package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import nceductimetable.demo.auth.Role;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class AccountAuth implements UserDetails {
    private int id;
    private String username;
    private List<Role> authorities;
    private String password;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
}
