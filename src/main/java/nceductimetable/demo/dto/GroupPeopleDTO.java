package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class GroupPeopleDTO {

    private String name;
    private String description;
    private List<AccountForGroups> members;
}
