package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import nceductimetable.demo.model.Account;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class TimePicking {
    /*private LocalDateTime begin;
    private LocalDateTime end;
    private List<Integer> accounts;*/
    private List<UserTime> events;
    private Integer priority;
    private LocalDateTime begin;
    private LocalDateTime end;
}
