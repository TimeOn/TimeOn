package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
public class AccountRegister {

    private String email;
    private String login;
    private String password;
    private String name;
    private String surname;
}
