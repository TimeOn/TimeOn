package nceductimetable.demo.dto;

import lombok.Data;

@Data
public class AccountIsBusy {
    private String name;
    private String surname;
}
