package nceductimetable.demo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
@ToString
public class AccountEmployment {
    private String firstName;
    private String lastName;
    private LocalDateTime time;
}
