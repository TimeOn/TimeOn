package nceductimetable.demo.dto;

import lombok.Data;

import java.util.List;

@Data
public class GroupPeopleDTOGet {
    private int id;
    private String name;
    private String description;
    private List<AccountForGroups> members;
}
