package nceductimetable.demo.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "EVENT_EXTERNAL_ACCOUNTS")
@Data
public class EventExternalAccounts {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "isImportant")
    private boolean isImportant;

    @Column(name = "status")
    private String status;

    @Column(name = "externalEmail")
    private String externalEmail;

    @Column(name = "externalSystem")
    private String externalSystem;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;


}
