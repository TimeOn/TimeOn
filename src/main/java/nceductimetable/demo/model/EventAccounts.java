package nceductimetable.demo.model;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "EVENT_ACCOUNTS")
@Data
public class EventAccounts {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "isImportant")
    private boolean isImportant;

    @Column(name = "status")
    private String status;// String


    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;


}
