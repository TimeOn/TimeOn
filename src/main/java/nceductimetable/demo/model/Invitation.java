package nceductimetable.demo.model;

import javax.persistence.*;

import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Table(name = "INVITATION")
@Data
public class Invitation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "text")
    private String text;

    @ManyToOne
    @JoinColumn(name = "idAccount", nullable = false)
    private Account account;

}
