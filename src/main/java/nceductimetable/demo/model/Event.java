package nceductimetable.demo.model;

import javax.persistence.*;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

@Entity

@Table(name = "EVENT")
@Data

public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;
/*
    @Column(name = "parent_id")
    private Event parent;*/

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "dateTimeBegin", columnDefinition= "TIMESTAMP WITHOUT TIME ZONE")
    private LocalDateTime dateTimeBegin;

    /*@Column(name = "dateTimeEnd")
    private ZonedDateTime dateTimeEnd;
*/
    /*@Column(name = "timezone")
    private TimeZone timezone;*/

    @Column(name="duration")
    private Long duration;

    @Column(name = "description")
    private String description;

    @Column(name = "isCancelled")
    private boolean isCancelled;

    @Column(name = "creation_dateTime", columnDefinition= "TIMESTAMP WITHOUT TIME ZONE")
    private LocalDateTime creationDataTime;



    @Column(name = "recurrence")
    private String recurrence;

    @Column(name = "isAllDay")
    private boolean isAllDay;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    private List<CalendarEvent> calendarEvents;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    private List<EventAccounts> membersAccounts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    private List<EventGroups> membersGroups;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    private List<EventExternalAccounts> membersExternalAccounts;

    //public Event createAndCopyEvent();
}
