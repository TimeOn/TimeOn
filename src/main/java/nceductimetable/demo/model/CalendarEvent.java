package nceductimetable.demo.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "CALENDAR_EVENT")
@Data
public class CalendarEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "external_event_id")
    private String externalEventId;

    @ManyToOne
    @JoinColumn(name = "calendar_id", nullable = false)
    private Calendar calendar;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;



}
