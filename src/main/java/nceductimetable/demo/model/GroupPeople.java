package nceductimetable.demo.model;

import javax.persistence.*;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "GROUP_PEOPLE")
@Data
public class GroupPeople {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "GROUP_ACCOUNT",
            joinColumns = {@JoinColumn(name = "idGroup")},
            inverseJoinColumns = {@JoinColumn(name = "idAccount"),
            }
    )
    private List<Account> members = new LinkedList<>();

    @ManyToOne
    @JoinColumn(name = "creator_id", nullable = false)
    private Account creator;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "group")
    private List<EventGroups> meetings;
}
