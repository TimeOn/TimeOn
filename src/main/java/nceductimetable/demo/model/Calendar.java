package nceductimetable.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CALENDAR")
@Data
public class Calendar {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "color")
    private String color;

    @Column(name = "external_id")
    private String externalId;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @ManyToOne
    @JoinColumn(name = "external_account_id")
    private ExternalAccount externalAccount;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "calendar")
    private List<CalendarEvent> calendarEvents;


}
