package nceductimetable.demo.model;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "EVENT_GROUPS")
@Data
public class EventGroups {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "isImportant")
    private boolean isImportant;

    @ManyToOne(/*cascade = CascadeType.ALL*/)
    @JoinColumn(name = "group_id")
    private GroupPeople group;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;


}
