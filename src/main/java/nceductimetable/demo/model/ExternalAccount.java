package nceductimetable.demo.model;

import lombok.Data;
import nceductimetable.demo.calendarSystem.oauth.TokenResponse;

import javax.persistence.*;
import java.util.List;
import java.util.Date;

@Entity
@Table(name = "EXTERNAL_ACCOUNT")
@Data
public class ExternalAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "external_id")
    private String externalId;

    @Column(name = "tenant_id", length=2000)
    private String tenantId;


    @Column(name = "external_system")
    private String externalSystem;

    @Column(name="user_name")
    private String userName;

    @Column(name="user_email")
    private String userEmail;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "externalAccount")
    private List<Calendar> calendars;

    @Column(name="token_type", length=2000)
    private String tokenType;

    @Column(name="scope",length=2000)
    private String scope;

    @Column(name="expres_in",length=2000)
    private int expiresIn;
    @Column(name="access_token",length=2000)
    private String accessToken;
    @Column(name="refresh_token",length=2000)
    private String refreshToken;
    @Column(name="id_token",length=2000)
    private String idToken;

    /*@Column(name="error")
    private String error;
    @Column(name="error_description")
    private String errorDescription;
    @Column(name="error_codes")
    private int[] errorCodes;*/
    @Column(name="expiration_date")
    private Date expirationTime;

    public TokenResponse getToken(){
        TokenResponse tokenResponse=new TokenResponse();
        tokenResponse.setAccessToken(this.accessToken);
        tokenResponse.setExpiresIn(this.expiresIn);
        tokenResponse.setIdToken(this.idToken);
        tokenResponse.setRefreshToken(this.refreshToken);
        tokenResponse.setScope(this.scope);
        tokenResponse.setTokenType(this.tokenType);
        tokenResponse.setExpirationTime(this.expirationTime);
        return tokenResponse;
    }

    public void setToken(TokenResponse token){
        this.setAccessToken(token.getAccessToken());
        this.setExpiresIn(token.getExpiresIn());
        this.setIdToken(token.getIdToken());
        this.setRefreshToken(token.getRefreshToken());
        this.setScope(token.getScope());
        this.setTokenType(token.getTokenType());
        this.setExpirationTime(token.getExpirationTime());
    }

}
