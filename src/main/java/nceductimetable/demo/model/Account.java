package nceductimetable.demo.model;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "ACCOUNT")
@Data
public class Account {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "avatar_url")
    private String avatarUrl;

    /*@Column (name="timezone")
    private String timezone;*/

    @Column(name = "isVisible")
    private boolean isVisible;

    @Column(name = "email")
    private String email;

    @Column(name = "isActivated")
    private boolean isActivated;

    @Column(name = "creationDate")
    private LocalDateTime creationDate;

    @Column(name = "activate")
    private String activate;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "members")
    private List<GroupPeople> groups;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "creator")
    private List<GroupPeople> myGroups;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<Invitation> myInvitations;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<ExternalAccount> externalAccounts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<Calendar> calendars;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<EventAccounts> meetingsAccounts;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "FRIENDS",
            joinColumns = {@JoinColumn(name = "PERSON_ID")},
            inverseJoinColumns = {@JoinColumn(name = "FRIEND_ID")})
    private List<Account> persons;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "persons")
    private List<Account> friends;

}
